var pub = pub || {};//public命名空间
var exp01 = exp01||{};//每一节一个命名空间
var exp02 = exp02||{};
var exp03 = exp03||{};
var exp04 = exp04||{};
var exp05 = exp05||{};
var exp06 = exp06||{};
var exp07 = exp07||{};
var exp08 = exp08||{};
var exp09 = exp09||{};
var exp10 = exp10||{};
var exp11 = exp11||{};
var exp12 = exp12||{};
var exp13 = exp13||{};
var exp14 = exp14||{};
var exp15 = exp15||{};
var exp16 = exp16||{};
var exp17 = exp17||{};
var exp18 = exp18||{};
var exp19 = exp19||{};
var exp20 = exp20||{};
/**
 * 权限控制
 */
ch = {
		addPublicRes:/**
		 * 添加公共资源
		 */
			function(){
			cc.spriteFrameCache.addSpriteFrames(res_public.common_p);
			cc.spriteFrameCache.addSpriteFrames(res_public.login_p);
		},
		howToGo:function(opType){
			switch(opType){
			case OP_TYPE_BACK:
				// 返回
				if(uu.loginType == LOGIN_TYPE_APP){
					$.runScene(new SelectScene());// 选择页面
				} else if(uu.loginType == LOGIN_TYPE_PLATFORM) {
					if(cc.sys.platform == cc.sys.DESKTOP_BROWSER){
						// TODO 结束应用关闭页面
						history.go(-1);
					} else if(cc.sys.os == "Android"
						|| cc.sys.os == "Windows"){
						cc.director.end();// 结束应用返回平台
					} else if(cc.sys.os == "IOS"){
						// TODO 结束应用返回平台
					}
				}
				break;
			case OP_TYPE_LOGIN:
				if(uu.loginType == LOGIN_TYPE_APP){
					// 从图标(登录页面)进入,加载所有资源
					loadResHand.startLoad();
					$.runScene(new LoginScene());
				} else if(uu.loginType == LOGIN_TYPE_PLATFORM){
					// 平台进入,加载对应实验的资源
					loadResHand.startLoad(uu.expId);
					this.getExpInfo(uu.expId);
					this.run(uu.info.expTag);
				}
				break;
			case OP_TYPE_SELECT:
				$.runScene(new SelectScene());
				break;
			default:
				break;
			}
		},
		getExpInfo: /**
		 * 完善用户的实验信息
		 * 
		 * @param expId
		 */
			function(expId){
			for(var i in expInfo){
				var info = expInfo[i];
				if(info.expId == expId){
					uu.info = info;
					return;
				}
			}
		},
		run:/**
		 * 运行实验
		 * 
		 * @param expId
		 */
			function(tag){
			for(var i in expInfo){
				var exp = expInfo[i];
				if(exp.expTag == tag){
					exp.run();
					break;
				}
			}
			cc.log("进入tag::" + tag);
		},
		gotoGame:/**
		 * 进入趣味游戏
		 * 
		 * @param expTag
		 *            实验标签
		 * @param expRes
		 *            私有资源
		 * @param spriteFrame
		 *            私有Frame
		 */
			function(expTag, expRes, spriteFrame){
			if(loadResHand.checkLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_GAME)
					&& loadResHand.checkLoadFinish(expTag, TAG_LOAD_TYPE_GAME)){
				if(!!spriteFrame){
					cc.spriteFrameCache.addSpriteFrames(spriteFrame);
				}
				$.pushScene(new GameScene());
			}else {
//				var preRes = [];
//				if(!!expRes){
//				preRes = g_resources_public_game.concat(expRes);
//				} else {
//				preRes = g_resources_public_game;
//				}
//				cc.LoaderScene.preload(preRes, function () {
//				if(!!spriteFrame){
//				cc.spriteFrameCache.addSpriteFrames(spriteFrame);
//				}
//				$.pushScene(new GameScene());
//				}, this);
				this.showTip("正在加载资源,请稍后");
			}
		},
		gotoTest:/**
		 * 调整到试题页面
		 * 
		 * @param expTag
		 *            实验标签
		 * @param expRes
		 *            私有资源
		 * @param spriteFrame
		 *            私有Frame
		 * @param param
		 *            私有参数
		 */
			function(expTag, expRes, spriteFrame, param){
			if(loadResHand.checkLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_TEST)){
				$.pushScene(new TestScene(param));
			}  else {
//				cc.LoaderScene.preload(g_resources_public_test, function () {
//				$.pushScene(new TestScene(param));
//				}, this);
				this.showTip("正在加载资源,请稍后");
			}
		},
		gotoAbout:/**
		 * 跳转到关于页面
		 * 
		 * @param expTag
		 *            实验标签
		 * @param expRes
		 *            私有资源
		 * @param spriteFrame
		 *            私有Frame
		 * @param param
		 *            私有参数
		 */
			function(expTag, expRes, spriteFrame, param){
			if(loadResHand.checkLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_ABOUT)){
				$.pushScene(new AboutScene(param));
			}  else {
//				cc.LoaderScene.preload(g_resources_public_about, function () {
//				$.pushScene(new AboutScene(param));
//				}, this);
				this.showTip("正在加载资源,请稍后");
			}
		},
		gotoStart:/**
		 * 进入开始页面
		 * 
		 * @param expTag
		 *            实验标签
		 * @param expRes
		 *            私有资源
		 * @param spriteFrame
		 *            私有Frame
		 */
			function(expTag, expRes, spriteFrame){
			gg.runExpTag = expTag;
			if(loadResHand.checkLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_START)
					&& loadResHand.checkLoadFinish(expTag, TAG_LOAD_TYPE_START)){
				if(!!spriteFrame){
					// 清空上次缓存
					if(!!gg.curStartSpriteFrame){
						cc.spriteFrameCache.removeSpriteFramesFromFile(gg.curStartSpriteFrame);
					}
					gg.curStartSpriteFrame = spriteFrame;
					// 加载缓存
					cc.spriteFrameCache.addSpriteFrames(spriteFrame);
				}
				$.runScene(new StartScene());
			}else {
				var preRes = [];
				if(!!expRes){
					preRes = g_resources_public_start.concat(expRes);
				} else {
					preRes = g_resources_public_start;
				}
				cc.LoaderScene.preload(preRes, function () {
					if(!!spriteFrame){
						if(!!gg.curStartSpriteFrame){
							cc.spriteFrameCache.removeSpriteFramesFromFile(gg.curStartSpriteFrame);
						}
						gg.curStartSpriteFrame = spriteFrame;
						cc.spriteFrameCache.addSpriteFrames(spriteFrame);
					}
					loadResHand.setLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_START);
					loadResHand.setLoadFinish(expTag, TAG_LOAD_TYPE_START);
					$.runScene(new StartScene());
				}, this);
			}
		},
		gotoRun : /**
		 * 跳转到运行页面
		 * 
		 * @param expTag
		 *            实验标签
		 * @param expRes
		 *            私有资源
		 * @param spriteFrame
		 *            私有Frame
		 * @param scene
		 *            场景
		 */
			function(expTag,MenuTag, expRes, spriteFrame, scene){
			gg.runMenuTag = MenuTag;
			if(loadResHand.checkLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_RUN)
					&&loadResHand.checkLoadFinish(expTag, TAG_LOAD_TYPE_RUN)){
				cc.spriteFrameCache.addSpriteFrames(res_public_run.run_p);
				for(var i in gg.curRunSpriteFrame){
					// 清楚图片缓存
					var sf = gg.curRunSpriteFrame[i];
					cc.spriteFrameCache.removeSpriteFramesFromFile(sf);
				}
				gg.curRunSpriteFrame = [];
				$.runScene(scene);
			}  else {
				var preRes = [];
				if(!!expRes){
					preRes = g_resources_public_run.concat(expRes);
				} else {
					preRes = g_resources_public_run;
				}
				cc.LoaderScene.preload(preRes, function () {
					loadResHand.setLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_RUN);
					loadResHand.setLoadFinish(expTag, TAG_LOAD_TYPE_RUN);
					cc.spriteFrameCache.addSpriteFrames(res_public_run.run_p);
					for(var i in gg.curRunSpriteFrame){
						// 清楚图片缓存
						var sf = gg.curRunSpriteFrame[i];
						cc.spriteFrameCache.removeSpriteFramesFromFile(sf);
					}
					gg.curRunSpriteFrame = [];
					$.runScene(scene);
				}, this);
			}
		},
		gotoShow : /**
		 * 跳转到展示页面
		 * 
		 * @param expTag
		 *            实验标签
		 * @param expRes
		 *            私有资源
		 * @param spriteFrame
		 *            私有Frame
		 * @param scene
		 *            场景
		 */
			function(expTag,MenuTag, expRes, spriteFrame, scene){
			gg.runMenuTag = MenuTag;
			if(loadResHand.checkLoadFinish(expTag, TAG_LOAD_TYPE_SHOW)){
			//	cc.spriteFrameCache.addSpriteFrames(res_public_run.run_p);
				for(var i in gg.curRunSpriteFrame){
					// 清楚图片缓存
					var sf = gg.curRunSpriteFrame[i];
					cc.spriteFrameCache.removeSpriteFramesFromFile(sf);
				}
				gg.curRunSpriteFrame = [];
				$.runScene(scene);
			}  else {
				var preRes = [];
				if(!!expRes){
					//preRes = g_resources_public_run.concat(expRes);
				} else {
					//preRes = g_resources_public_run;
				}
				cc.LoaderScene.preload(preRes, function () {
					//loadResHand.setLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_RUN);
					loadResHand.setLoadFinish(expTag, TAG_LOAD_TYPE_SHOW);
					//cc.spriteFrameCache.addSpriteFrames(res_public_run.run_p);
					for(var i in gg.curRunSpriteFrame){
						// 清楚图片缓存
						var sf = gg.curRunSpriteFrame[i];
						cc.spriteFrameCache.removeSpriteFramesFromFile(sf);
					}
					gg.curRunSpriteFrame = [];
					$.runScene(scene);
				}, this);
			}
		},
		gotoFinish : /**
		 * 跳转到结束页面
		 */
			function(){
			var running = cc.director.getRunningScene();
			running.scheduleOnce(function(){
				if(loadResHand.checkLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_FINISH)){
					$.runScene(new FinishScene(function(){
						_.stop();
						ch.run(gg.runExpTag);
					}));
				}  else {
					cc.LoaderScene.preload(g_resources_public_finish, function () {
						$.runScene(new FinishScene(function(){
							_.stop();
							loadResHand.setLoadFinish(TAG_EXP_PUBLIC, TAG_LOAD_TYPE_FINISH);
							ch.run(gg.runExpTag);
						}));
					}, this);
				}
			},2);
		},
		showTip:/**
		 * 提示
		 * 
		 * @param str
		 */
			function(str){
			var st = new ShowTipPublic();
			st.setPosition(gg.c_p);
			st.init(str, 1);
		}
}

/**
 * 异步加载资源
 */
loadResHand = {
		startLoad:/**
		 * 开始加载资源
		 * 
		 * @param expId
		 *            如果带实验id,则只加载一个实验的资源
		 */
			function(expId){
			for(var i in expInfo){
				var exp = expInfo[i];
				if(!!expId && exp.expId != expId && exp.expId > 0){
					// 如果有实验ID,就不加载其他实验(公共部分还是要加载的)
					continue;
				}
				for(var j in exp.loadRes){
					var res = exp.loadRes[j];
					if (cc.sys.platform != cc.sys.DESKTOP_BROWSER) {
						res.finish = true;
						return;
					} else {
						if(res.finish){
							cc.log(res.expName + ":已完成,不需要加载");
							continue;
						}
						if(res.type == TAG_LOAD_TYPE_START
								||res.type == TAG_LOAD_TYPE_RUN
//								||res.type == TAG_LOAD_TYPE_FINISH//结束页单独拿出来了
						){
							// cocos2d BUG cc.loader.load多次加载同一资源BUG
							continue;
						}
						res.expName = exp.expName;
						cc.loader.load(res.res,
								function () {
							this.finish = true;
							if(this.type == TAG_LOAD_TYPE_START){
								cc.log(this.expName + ":开始页面加载完成");
							} else if(this.type == TAG_LOAD_TYPE_RUN){
								cc.log(this.expName + ":仿真页面加载完成");
							} else if(this.type == TAG_LOAD_TYPE_FINISH){
								cc.log(this.expName + ":结束页面加载完成");
							} else if(this.type == TAG_LOAD_TYPE_GAME){
								cc.log(this.expName + ":游戏页面加载完成");
							} else if(this.type == TAG_LOAD_TYPE_ABOUT){
								cc.log(this.expName + ":关于页面加载完成");
							} else if(this.type == TAG_LOAD_TYPE_TEST){
								cc.log(this.expName + ":试题页面加载完成");
							}
						}.bind(res));
					}
				}
			}
		},
		checkLoadFinish:/**
		 * 检查是否加载完成
		 * 
		 * @param expTag
		 *            实验标签
		 * @param type
		 *            资源类型
		 * @returns {Boolean}
		 */
			function(expTag, type){
			if (cc.sys.platform != cc.sys.DESKTOP_BROWSER) {
				return true;
			}
			if(!expTag || !type){
				return true;
			}
			for(var i in expInfo){
				var exp = expInfo[i];
				if(exp.expTag != expTag){
					continue;
				}
				for(var j in exp.loadRes){
					var res = exp.loadRes[j];
					if(res.finish 
							&& type == res.type){
						cc.log(exp.expName + ":加载完成");
						return true;
					}
				}
			}
			return false;
		},
		setLoadFinish:/**
		 * 设置加载完成
		 * 
		 * @param expTag
		 *            实验标签
		 * @param type
		 *            资源类型
		 * @returns {Boolean}
		 */
			function(expTag, type){
			if (cc.sys.platform != cc.sys.DESKTOP_BROWSER) {
				return true;
			}
			if(!expTag || !type){
				return true;
			}
			for(var i in expInfo){
				var exp = expInfo[i];
				if(exp.expTag != expTag){
					continue;
				}
				for(var j in exp.loadRes){
					var res = exp.loadRes[j];
					if(res.type != type){
						continue;
					}
					res.finish = true;
					cc.log("setLoadFinish||" + exp.expName + ":加载完成");
					return true;
				}
			}
			return false;
		}
}
