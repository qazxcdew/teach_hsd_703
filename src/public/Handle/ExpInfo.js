/**
 *实验配置信息
 */
expInfo = [
{
	expName:"公共部分",
	expTag:TAG_EXP_PUBLIC,
	expId:-1,
	loadRes:[{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_START,
		finish:false,
		res:g_resources_public_start
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_RUN,
		finish:false,
		res:g_resources_public_run
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_ABOUT,
		finish:false,
		res:g_resources_public_about
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_GAME,
		finish:false,
		res:g_resources_public_game
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_FINISH,
		finish:false,
		res:g_resources_public_finish
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_TEST,
		finish:false,
		res:g_resources_public_test
	},]
},

{
	expName: "1.4  运动与力",
	expTag: TAG_EXP_02,
	expId: 105,
	expimage:"#exp/exp02.png",
	chapterImg:"#chapter/chapter1.png",
	startPng:res_start02.start_png,
	expDemo:TAG_TYPE_CESHI,
	run: function(){
		gg.runNext = exp02.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp02.teach_flow01);
		ch.gotoStart(this.expTag, this.loadRes[0].res, res_start02.start_p);
	},
	menuItem :[
	           {
	        	   menu:"牛顿第一定律",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"小车运动实验",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"惯性",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"力是改变物体运动状态的原因",
	        	   tag:TAG_MENU4 
	           }	        
	           
	           ],
	loadRes:[{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_START,
		finish:false,
		res:g_resources_start02
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_RUN,
		finish:false,
		res:g_resources_run02
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_FINISH,
		finish:false,
		res:[]
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_SHOW,
		finish:false,
		res:g_resources_show02
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_ABOUT,
		finish:false,
		res:[]
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_TEST,
		finish:false,
		res:[]
	}]
},
{
	expName: "2.3  大气压强",
	expTag: TAG_EXP_01,
	expId: 106,
	expimage:"#exp/exp01.png",
	chapterImg:"#chapter/chapter2.png",
	startPng:res_start01.start_png,
	YuanLi:"1、浸在液体里的物体，受到向上的浮力，浮" +
	"\n     力的大小等于物体排开的液体受到的重力",
	MuDi:"1、通过实验验证阿基米德原理的正确性" +
	"\n2、加深对阿基米德原理的理解" +
	"\n3、培养学生的实验操作能力",
	expDemo : null,
	run: function(){
		gg.runNext = exp01.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp01.teach_flow01);
		ch.gotoStart(this.expTag, this.loadRes[0].res, res_start01.start_p );
	},
	menuItem :[
	           {
	        	   menu:"大气压的测定",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"大气压强的变化",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"大气压强的应用",
	        	   tag:TAG_MENU3 
	           }         
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:g_resources_start01
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:g_resources_run01
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:g_resources_show01
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_ABOUT,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_TEST,
	        	   finish:false,
	        	   res:[]
	           }]
},
{
	expName: "3.4  物体浮沉条件及其应用" ,
	expTag: TAG_EXP_03,
	expId: 107,
	expimage:"#exp/exp03.png",
	chapterImg:"#chapter/chapter3.png",
	startPng:res_start03.start_png,
	run: function(){
		gg.runNext = exp03.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp03.teach_flow01);
		ch.gotoStart(this.expTag, this.loadRes[0].res, res_start03.start_p);
	},
	menuItem :[
	           {
	        	   menu:"物体在液体中的浮沉条件",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"浮沉条件的应用",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"探究潜艇沉浮的道理",
	        	   tag:TAG_MENU3 
	           }	                 
	           ],
	loadRes:[{
		expTag:TAG_EXP_03,
		type:TAG_LOAD_TYPE_START,
		finish:false,
		res:g_resources_start03
	},{
		expTag:TAG_EXP_03,
		type:TAG_LOAD_TYPE_RUN,
		finish:false,
		res:g_resources_run03
	},{
		expTag:TAG_EXP_03,
		type:TAG_LOAD_TYPE_FINISH,
		finish:false,
		res:[]
	},{
		expTag:TAG_EXP_03,
		type:TAG_LOAD_TYPE_SHOW,
		finish:false,
		res:g_resources_show03
	},{
		expTag:TAG_EXP_03,
		type:TAG_LOAD_TYPE_TEST,
		finish:false,
		res:[]
	}]
},
//{
//expName: "对流实验",
//expTag: TAG_EXP_04,
//expId: 87,
//expimage:"#exp/exp04.png",
//YuanLi:"1、空气对流是由于空气受热不均，受热的空气膨" +
//"\n    胀上升，而受冷的空气下沉而形成的。",
//MuDi:"1、验证大气对流运动是由空气受热不均引起的",
//expDemo:null,
//run: function(){
//gg.runNext = startRunNext04;
//gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, teach_flow04);
//ch.gotoStart(this.expTag, this.loadRes[0].res, res_start04.start_p);
//},
//loadRes:[{
//expTag:TAG_EXP_04,
//type:TAG_LOAD_TYPE_START,
//finish:false,
//res:g_resources_start04
//},{
//expTag:TAG_EXP_04,
//type:TAG_LOAD_TYPE_RUN,
//finish:false,
//res:g_resources_run04
//},{
//expTag:TAG_EXP_04,
//type:TAG_LOAD_TYPE_FINISH,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_04,
//type:TAG_LOAD_TYPE_GAME,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_04,
//type:TAG_LOAD_TYPE_TEST,
//finish:false,
//res:[]
//}]
//},
//{
//expName: "人体内分泌腺的" +
//"\n    分布与功能",
//expTag: TAG_EXP_05,
//expId: 88,
//expimage:"#exp/exp05.png",
//YuanLi:"1、人体内分泌腺的由垂体、甲状腺、肾上腺、" +
//"\n胰腺、睾丸（男性）和卵巢（女性）组成" ,

//MuDi:"1、熟悉人体内分泌腺的组成器官" +
//"\n2、知道各个器官的作用",
//expDemo:TAG_TYPE_YANSHI,
//run: function(){
//gg.runNext = startRunNext05;
//gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, teach_flow05);
//ch.gotoStart(this.expTag, this.loadRes[0].res, res_start05.start_p);
//},
//loadRes:[{
//expTag:TAG_EXP_05,
//type:TAG_LOAD_TYPE_START,
//finish:false,
//res:g_resources_start05
//},{
//expTag:TAG_EXP_05,
//type:TAG_LOAD_TYPE_RUN,
//finish:false,
//res:g_resources_run05
//},{
//expTag:TAG_EXP_05,
//type:TAG_LOAD_TYPE_FINISH,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_05,
//type:TAG_LOAD_TYPE_GAME,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_05,
//type:TAG_LOAD_TYPE_TEST,
//finish:false,
//res:[]
//}]
//},


//{
//expName: "大闹皮层不同" +
//"\n    的功能区",
//expTag: TAG_EXP_06,
//expId: 89,
//expimage:"#exp/exp06.png",
//YuanLi:"1、大脑皮层是神经元的细胞体高度集中的地方" +
//"\n     有许多控制人身体活动的高级中枢" +
//"\n2、大脑皮层还能对语言和文字发生反应，因而具有" +
//"\n     抽象、概括、推理、计算、想想等思维能力",
//MuDi:"1、了解大脑皮层不同区域不同的功能" +
//"\n2、了解大脑对我们人体的重要性",
//expDemo:TAG_TYPE_YANSHI,
//run: function(){
//gg.runNext = startRunNext06;
//gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, teach_flow06);
//ch.gotoStart(this.expTag, this.loadRes[0].res, res_start06.start_p);
//},
//loadRes:[{
//expTag:TAG_EXP_06,
//type:TAG_LOAD_TYPE_START,
//finish:false,
//res:g_resources_start06
//},{
//expTag:TAG_EXP_06,
//type:TAG_LOAD_TYPE_RUN,
//finish:false,
//res:g_resources_run06
//},{
//expTag:TAG_EXP_06,
//type:TAG_LOAD_TYPE_FINISH,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_06,
//type:TAG_LOAD_TYPE_GAME,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_06,
//type:TAG_LOAD_TYPE_TEST,
//finish:false,
//res:[]
//}]
//},
//{
//expName: "缩手的反射过程",
//expTag: TAG_EXP_07,
//expId: 90,
//expimage:"#exp/exp07.png",
//YuanLi:"1、缩手反射是一种简单的多元反射" +
//"\n2、反射弧包括感受器、传入神经、\n神经中枢、传出神经、效应器",
//MuDi:"1、掌握缩手反射的整个过程" +
//"\n2、掌握反射弧相关知识",
//expDemo:TAG_TYPE_CESHI,
//run: function(){
//gg.runNext = startRunNext07;
//gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, teach_flow07);
//ch.gotoStart(this.expTag, this.loadRes[0].res, res_start07.start_p);
//},
//loadRes:[{
//expTag:TAG_EXP_07,
//type:TAG_LOAD_TYPE_START,
//finish:false,
//res:g_resources_start07
//},{
//expTag:TAG_EXP_07,
//type:TAG_LOAD_TYPE_RUN,
//finish:false,
//res:g_resources_run07
//},{
//expTag:TAG_EXP_07,
//type:TAG_LOAD_TYPE_TEST,
//finish:false,
//res:g_resources_test07
//}]
//},
//{
//expName: "水蒸气蒸馏法",
//expTag: TAG_EXP_08,
//expId: 28,
//expimage:"#exp/exp08.png",
//YuanLi:"1、  水加热到100℃变成水蒸气\n2、  水蒸气遇冷液化成水",
//MuDi:"1、  了解和掌握蒸馏装置的操作规范。",
//expDemo:null,
//run: function(){
//gg.runNext = startRunNext08;
//gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, teach_flow08);
//ch.gotoStart(this.expTag, this.loadRes[0].res, res_start08.start_p);
//},
//loadRes:[{
//expTag:TAG_EXP_08,
//type:TAG_LOAD_TYPE_START,
//finish:false,
//res:g_resources_start08
//},{
//expTag:TAG_EXP_08,
//type:TAG_LOAD_TYPE_RUN,
//finish:false,
//res:g_resources_run08
//},{
//expTag:TAG_EXP_08,
//type:TAG_LOAD_TYPE_FINISH,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_08,
//type:TAG_LOAD_TYPE_GAME,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_08,
//type:TAG_LOAD_TYPE_TEST,
//finish:false,
//res:[]
//}]
//},
//{
//expName: "水的电解",
//expTag: TAG_EXP_09,
//expId: 56,
//expimage:"#exp/exp09.png",
//YuanLi:"1、  认识水是由氢、氧两种元素组成的；\n2、  验证水在通电条件下，生成氢气和\n     氧气。",
//MuDi:"1、  水是由氢、氧两种元素组成；\n2、  水（通电直流） →  氢气 + 氧气。",
//expDemo:null,
//run: function(){
//gg.runNext = startRunNext09;
//gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, teach_flow09);
//ch.gotoStart(this.expTag, this.loadRes[0].res, res_start09.start_p);
//},
//loadRes:[{
//expTag:TAG_EXP_09,
//type:TAG_LOAD_TYPE_START,
//finish:false,
//res:g_resources_start09
//},{
//expTag:TAG_EXP_09,
//type:TAG_LOAD_TYPE_RUN,
//finish:false,
//res:g_resources_run09
//},{
//expTag:TAG_EXP_09,
//type:TAG_LOAD_TYPE_FINISH,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_09,
//type:TAG_LOAD_TYPE_GAME,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_09,
//type:TAG_LOAD_TYPE_TEST,
//finish:false,
//res:[]
//}]
//},
//{
//expName: "粗盐提纯实验",
//expTag: TAG_EXP_10,
//expId: 59,
//expimage:"#exp/exp10.png",
//YuanLi:"1、  了解用过滤的方法除去粗盐中不溶性杂\n     质;\n2、  学习溶解、过滤、蒸发等基本操作。",
//MuDi:"1、不溶性的物质，通过溶解、过滤去除。",
//expDemo:null,
//run: function(){
//gg.runNext = startRunNext10;
//gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, teach_flow10);
//ch.gotoStart(this.expTag, this.loadRes[0].res, res_start10.start_p);
//},
//loadRes:[{
//expTag:TAG_EXP_10,
//type:TAG_LOAD_TYPE_START,
//finish:false,
//res:g_resources_start10
//},{
//expTag:TAG_EXP_10,
//type:TAG_LOAD_TYPE_RUN,
//finish:false,
//res:g_resources_run10
//},{
//expTag:TAG_EXP_10,
//type:TAG_LOAD_TYPE_FINISH,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_10,
//type:TAG_LOAD_TYPE_GAME,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_10,
//type:TAG_LOAD_TYPE_TEST,
//finish:false,
//res:[]
//}]
//},
//{
//expName: "气压对液体沸" +
//"\n    点的影响",
//expTag: TAG_EXP_11,
//expId: 31,
//expimage:"#exp/exp11.png",
//YuanLi:"1、液体的沸点与气压有关：当气压增大时，" +
//"\n     液体的沸点会升高" +
//"\n2、液体的沸点与气压有关：当气压减小时，" +
//"\n     液体的沸点会降低",
//MuDi:"1、确认大气压强的存在。" +
//"\n2、了解液体的沸点跟表面气压的关系" +
//"\n3、解释有关现象和解决简单的实际问题" +
//"\n4、体验大气压强对沸点的影响",
//expDemo:null,
//run: function(){
//gg.runNext = startRunNext11;
//gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, teach_flow11);
//ch.gotoStart(this.expTag, this.loadRes[0].res, res_start11.start_p);
//},
//loadRes:[{
//expTag:TAG_EXP_11,
//type:TAG_LOAD_TYPE_START,
//finish:false,
//res:g_resources_start11
//},{
//expTag:TAG_EXP_11,
//type:TAG_LOAD_TYPE_RUN,
//finish:false,
//res:g_resources_run11
//},{
//expTag:TAG_EXP_11,
//type:TAG_LOAD_TYPE_FINISH,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_11,
//type:TAG_LOAD_TYPE_GAME,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_11,
//type:TAG_LOAD_TYPE_TEST,
//finish:false,
//res:[]
//}]
//},
//{
//expName: "植被护坡",
//expTag: TAG_EXP_12,
//expId: 55,
//expimage:"#exp/exp12.png",
//YuanLi:"1、  植被能保持水土，增加雨水下渗;\n2、 破坏植被会使洪水的危害增大。",
//MuDi:"1、验证植被护坡的重要性;\n2、学习生活中植被覆盖率的实际意义。",
//expDemo:TAG_TYPE_CESHI,
//run: function(){
//gg.runNext = startRunNext12;
//gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, teach_flow12);
//ch.gotoStart(this.expTag, this.loadRes[0].res, res_start12.start_p);
//},
//loadRes:[{
//expTag:TAG_EXP_12,
//type:TAG_LOAD_TYPE_START,
//finish:false,
//res:g_resources_start12
//},{
//expTag:TAG_EXP_12,
//type:TAG_LOAD_TYPE_RUN,
//finish:false,
//res:g_resources_run12
//},{
//expTag:TAG_EXP_12,
//type:TAG_LOAD_TYPE_FINISH,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_12,
//type:TAG_LOAD_TYPE_GAME,
//finish:false,
//res:[]
//},{
//expTag:TAG_EXP_12,
//type:TAG_LOAD_TYPE_TEST,
//finish:false,
//res:[]
//}]
//}
]
