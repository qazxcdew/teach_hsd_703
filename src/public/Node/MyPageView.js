/**
 * PageView
 */

MyPageView =  ccui.PageView.extend({
	ctor:function(){
		this._super();
	},
	getPages:function(){
		return this._pages;
	},
	scrollToPage: function (idx) {
		if (idx < 0 || idx >= this.getPages().length)
			return;
		this._curPageIdx = idx;
		var curPage = this.getPages()[idx];
		this._autoScrollDistance = -(curPage.getPosition().x);
		this._autoScrollSpeed = Math.abs(this._autoScrollDistance) / 0.5;
		this._autoScrollDirection = this._autoScrollDistance > 0 ? ccui.PageView.DIRECTION_RIGHT : ccui.PageView.DIRECTION_LEFT;
		this._isAutoScrolling = true;
	},
	_handleReleaseLogic: function (touchPoint) {
		if (this.getPages().length <= 0){
			return;
		}			
		var curPage = this.getPages()[this._curPageIdx];
		if (curPage) {
			var curPagePos = curPage.getPosition();
			var pageCount = this.getPages().length;
			var curPageLocation = curPagePos.x;
			var pageWidth = this.getSize().width;
			var boundary = pageWidth / 10.0;
			if (curPageLocation <= -boundary) {
				if (this._curPageIdx >= pageCount - 1)
					this._scrollPages(-curPageLocation);
				else
					this.scrollToPage(this._curPageIdx + 1);
			} else if (curPageLocation >= boundary) {
				if (this._curPageIdx <= 0)
					this._scrollPages(-curPageLocation);
				else
					this.scrollToPage(this._curPageIdx - 1);
			} else
				this.scrollToPage(this._curPageIdx);
		}
	},

})

