/**
 * 缩放按钮
 */
ButtonScale = cc.Sprite.extend({
	p:null,
	callback:null,
	back:null,
	enable:true,
	margin:0,
	normalName:null,
	hoverName:null,
	rectByChild:false,
	ctor:function (parent, fileName, callback, back, rect, rotated) {
		this._super(fileName, rect, rotated);
		this.p = parent;
		this.normalName = fileName.split("#")[1];
		this.hoverName = fileName.split("#")[1];
		this.orig_color = this.getColor();
		this.callback = callback;
		if(back == null){
			this.back = this.p;
		} else {
			this.back = back;
		}
		// 添加到界面
		this.p.addChild(this);
		// 添加按钮事件
		ButtonScaleListener.register(this);
	},
	setRectByChild:/**
					 * 点击范围 是否包括子节点范围 默认不包括
					 * 
					 * @param bool
					 */
		function(bool){
		this.rectByChild = bool;
	},
	genBox:/**
			 * 点击范围
			 * 
			 * @param reset
			 *            用于按钮是否发生位移
			 * @returns
			 */
		function(reset){
		if(!this.cbox || !!reset){ // 按钮缩小后,使用原来的点击范围
			if(this.rectByChild){
				this.cbox = this.getBoundingBoxToWorld();
			} else {
				var rect = cc.rect(0, 0, this.width, this.height);
				var trans = this.getNodeToWorldTransform();
				this.cbox = cc.rectApplyAffineTransform(rect, trans);
			}	
		}
		return this.cbox;
	},
	preCall:function(){
		// 回调之前的处理，一般子类才会用到
		
	},
	exeUnEnable:function(){
		// 不能点击
	},
	simCallBack:function(){
		if(this.callback != null){
			this.callback.call(this.back,this);	
		}
	},
	setTop:/**
			 * 把节点放到最上面
			 */
		function(){
		ButtonScaleListener.setTop(this);
	},
	setBottom:/**
				 * 把节点放到最下面
				 */
		function(){
		ButtonScaleListener.setBottom(this);
	},
	setEnable:function(enable){
		this.enable = enable;
	},
	isEnable:function(){
		return this.enable;
	},
	flash:function (){
		var fade1 = cc.fadeTo(0.5, 50);
		var fade2 = cc.fadeTo(0.5, 255);
		var seq = cc.sequence(fade1,fade2);
		var flash = cc.repeatForever(seq);
		this.runAction(flash);
	},
	stop:function (){
		this.stopAllActions();
		this.setOpacity(255);
		if(!$.isCanvs()){
			// 浏览器下的canvas渲染 对setColor的支持有BUG
			//item.setColor(cc.color(92, 92, 92));
			this.setColor(this.orig_color);
		}
		//this.setColor(this.orig_color);
	},
	onExit: function () {
		this._super();
		ButtonScaleListener.logOff(this);
	},
//	onEnter: function(){
//		this._super();
//		ButtonScaleListener.register(this);
//	},
	up:function (standard, margin){
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的y + 标准物的高度 * 缩放 * (1-锚点y) + 本身的高度 * 缩放 * 锚点y + 所需间隔
		var y = standard.y + standard.height * standard.getScaleY() * (1-sap.y)  + this.height * ap.y * this.getScaleY() + this.margin;
		this.setPosition(standard.x, y);
	},	
	down:function (standard, margin){
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
		var y = standard.y - standard.height * standard.getScaleY() * sap.y  - this.height * (1-ap.y) * this.getScaleY() - this.margin;
		this.setPosition(standard.x, y);
	},
	left:function (standard, margin){
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的x - 标准物的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * (1-锚点x) - 所需间隔
		var x = standard.x - standard.width * standard.getScaleX() * sap.x  - this.width * (1-ap.x) * this.getScaleX() - this.margin;
		this.setPosition(x, standard.y);
	},	
	right:function (standard, margin){
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的x + 标准物的宽度 * 缩放 * (1-锚点x) + 本身的宽度 * 缩放 * 锚点 + 需要间隔
		var x = standard.x + standard.width * standard.getScaleX() * (1-sap.x)  + this.width * ap.x * this.getScaleX() + this.margin;
		this.setPosition(x, standard.y);
	}
})

ANGEL_LISTENER_WAITING = 0;
ANGEL_LISTENER_TRACKING_TOUCH = 1;
/**
 * 点击监听辅助
 */
ButtonScaleListener = {
		regNode:[],
		register:function(node){
			this.regNode.push(node);
		},
		regListener:function(node){
			gg = gg || {};
			gg.scaleListener = gg.scaleListener == null ? 
					cc.EventListener.create({
						event: cc.EventListener.TOUCH_ONE_BY_ONE,
						swallowTouches: true,
						enable: true,// 点击不能用
						enableRoot: [],// Root节点下不能用
						clickThrough:false,// 内部透击
						state: ANGEL_LISTENER_WAITING,
						onTouchBegan: this.onTouchBegan,
						onTouchEnded: this.onTouchEnded,
						onTouchCancelled: this.onTouchEnded
					}) : gg.scaleListener.clone();
					gg.scaleListener.enable = true;
					gg.scaleListener.clickThrough = false;
					gg.scaleListener.state = ANGEL_LISTENER_WAITING;
					gg.scaleListener.retain();

					cc.log("ButtonScaleListener:注册事件");
					cc.eventManager.addListener(gg.scaleListener, node);
		},
		setEnable: function(enable){
			gg.scaleListener.enable = enable;
		},
		isEnable: function(){
			return gg.scaleListener.enable;
		},
		setTop: function(node){
			this.logOff(node);
			this.regNode.push(node);
		},
		setBottom: function(node){
			this.logOff(node);
			this.regNode.unshift(node);
		},
		logOff:function(node){
			for (var i = 0; i < this.regNode.length; i++) {
				if (this.regNode[i] == node) {
					this.regNode.splice(i, 1);
				}
			}
		},
		onTouchBegan: function(touch, event){
			if(this.state != ANGEL_LISTENER_WAITING 
					|| !this.enable){
				return false;
			}
			var item = ButtonScaleListener.itemForTouch(touch);
			if(!item){
				return false;
			}
			// 被点击则，进行缩放
			item.stopAllActions();
			var seq = cc.sequence(cc.scaleTo(0.1, 0.75),cc.scaleTo(0.05, 0.85),cc.scaleTo(0.05, 0.75));
			item.runAction(seq);
			if(!$.isCanvs()){
				// 浏览器下的canvas渲染 对setColor的支持有BUG
				item.setColor(cc.color(92, 92, 92));
			}
			//item.setColor(cc.color(92, 92, 92));
			
			this.state = ANGEL_LISTENER_TRACKING_TOUCH;
			var target = event.getCurrentTarget();
			target.item = item;
			return true;
		},
		onTouchEnded: function(touch, event){
			if(this.state != ANGEL_LISTENER_TRACKING_TOUCH){
				return;
			}
			var target = event.getCurrentTarget();
			var item = target.item;
			// 点击结束，更换常态图片
			item.stopAllActions();
			var seq = cc.sequence(cc.scaleTo(0.1, 1),cc.scaleTo(0.05, 0.9),cc.scaleTo(0.1, 1));
			item.runAction(seq);
			if(!$.isCanvs()){
				// 浏览器下的canvas渲染 对setColor的支持有BUG
				item.setColor(cc.color(255, 255, 255));
			}
//			item.setColor(cc.color(255, 255, 255));
			
			var pos = cc.p(touch.getLocationX(),touch.getLocationY());
			if(item.callback != null
				&& cc.rectContainsPoint(item.genBox(),pos)){
				item.preCall();
				// 有回调函数，则调用回调函数
				item.callback.call(item.back,item);
			}
			this.state = ANGEL_LISTENER_WAITING;
		},
		clickChose:/**
					 * 是否点中
					 * 
					 * @param node
					 */
			function(node, pos){
			return cc.rectContainsPoint(node.genBox(true),pos);
		},
		itemForTouch: /**
						 * 获取被点击的对象
						 * 
						 * @param touch
						 * @returns
						 */
			function (touch) {
			var unEnableObj = null;
			var touchLocation = touch.getLocation();
			if (this.regNode && this.regNode.length > 0) {
				for (var i = this.regNode.length - 1; i >= 0; i--) {
					var node = this.regNode[i];
					if(!this.isVisible(node)){
						continue;
					}
					var pos = cc.p(touch.getLocationX(),touch.getLocationY());
					if(!this.clickChose(node, pos)){
						continue;
					}
					if (node.isEnable() && !!node.callback) {
						return node;
					} else if(unEnableObj == null){
						unEnableObj = node;
					}
				}
			}
			if(unEnableObj != null){
				unEnableObj.exeUnEnable();
			}
			return null;
		},
		isVisible:/**
					 * 判断是否可见
					 * 
					 * @param node
					 */
			function(node){
			if(!node.isVisible() || node.getOpacity() == 0){
				return false;
			}
			for (var c = node; c != null; c = c.parent) {
				if(!c.isVisible() || c.getOpacity() == 0 && c.isCascadeOpacityEnabled()){
					return false;
				}
			}
			return true;
		}
}
