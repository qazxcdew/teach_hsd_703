var LibButton = Angel.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,zIndex,tag,normalImage, callback, back) {
		this._super(parent, "#lib/root.png", callback, back);
		this.setTag(tag);
		this.setLocalZOrder(zIndex);
		// 默认不能点击
		this.setEnable(false);
		this.setRectByChild(true);
		this.check = false;
		this.loadImage(normalImage);
		this.loadName();
//		this.loadFrame();
	},
	loadImage:function(name){
		var image = new cc.Sprite(name);
		var scale;
		if(image.width > image.height){
			scale = 100 / image.width;
		} else {
			scale = 100 / image.height;
		}
		image.setScale(scale);
		this.addChild(image);
	},
	loadName:function(){
		//增加遮罩
		var stencil = new cc.DrawNode()
		//模板范围，左下角坐标，右上角坐标，构成的矩形，默认可见true
		stencil.drawRect(cc.p(-90,-90),cc.p(100,55),cc.color(255,255,255,0),1,cc.color(255,255,255,0));
		var clipping = new cc.ClippingNode(stencil);
//		clipping.setInverted(true); //显示模板以外的
		this.addChild(clipping, 20,TAG_CLIPPING);
	//	clipping.setStencil(stencil);		

		var name = "";
		for(var i in libRelArr){
			if(libRelArr[i].tag == this.getTag()){
				name = libRelArr[i].name;
				break;
			}
		}
		this.nameLabel = new cc.LabelTTF(name, gg.fontName, 20);
		this.nameLabel.setColor(cc.color(255,255,255));
		//仪器名称背景
		this.word_bg = new cc.Sprite("#lib/word_bg.png");
		this.word_bg.setPosition(cc.p(0, -30 - 10 - 25 - this.word_bg.height));
		clipping.addChild(this.word_bg,2,TAG_WORD_BG);

		this.nameLabel.setPosition(this.word_bg.width*0.5,this.word_bg.height*0.5);
		this.word_bg.addChild(this.nameLabel,3,TAG_LABEL);	

		clipping.setCascadeOpacityEnabled(true);
		this.word_bg.setCascadeOpacityEnabled(true);
		this.nameLabel.setCascadeOpacityEnabled(true);
	},
	loadFrame:function(){

	},
	preCall:function(){
		cc.log("正确");
	},
	exeUnEnable:function(){
		cc.log("错误");
	},
	left:function (standard, margin){
		margin = 180;
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的x - 标准物的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * (1-锚点x) - 所需间隔
		var x = standard.x - standard.width * standard.getScaleX() * sap.x  - this.width * (1-ap.x) * this.getScaleX() - this.margin;
		this.setPosition(x, standard.y);
	},	
	right:function (standard, margin){
		margin = 180;
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 左边的x + 左边的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * 锚点
		var x = standard.x + standard.width * standard.getScaleX() * sap.x  + this.width * ap.x * this.getScaleX() + this.margin;
		this.setPosition(x, standard.y);
	},
	down:function (standard, margin){
		margin = 201;
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
		var y = standard.y - standard.height * standard.getScaleY() * sap.y  - this.height * (1-ap.y) * this.getScaleY() - this.margin;
		this.setPosition(standard.x, y);
	},	
})