var Button = Angel.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,zIndex,tag,normalImage, callback, back) {
		this._super(parent,normalImage, callback, back);
		this.setTag(tag);
		this.setLocalZOrder(zIndex);
		// 默认不能点击
		this.setEnable(false);
	},
	preCall:function(){
		// 隐藏箭头
		ll.tip.arr.out();
		//去除提示
		this.deleteShow();
		this.setEnable(false);
		//防止实验进行中，点击仪器，分数异常
		if(!gg.flow.over_flag){//实验未结束
			if(ll.run.lib != null){
				if((!ll.run.lib.isOpen()) && gg.scoreCheck){
					// 操作成功
					ll.tip.mdScore(10);
				}		
			}else{
				if(gg.scoreCheck){
					// 操作成功
					ll.tip.mdScore(10);
				}		
			}
			
		}
		
		
		if(!gg.errFlag){
			gg.oneSure ++;
		}
		gg.errFlag = false;
		_.clever();
	},
	exeUnEnable:function(){
		if(ll.run.lib != null){
			if((!ll.run.lib.isOpen()) && gg.scoreCheck){
				// 操作失败
				if(!gg.flow.over_flag){//判断是否结束
					ll.tip.mdScore(-3);
				}

			}	
		}
			
		gg.errFlag = true;
		gg.errorStep ++;
		_.error();
	},
	deleteShow:function(){
		if(ll.run == null){
			return;
		}
		var show = ll.run.getChildByTag(TAG_SHOW);
		if(show == null){
			return;
		}
		show.kill();
	}
})
