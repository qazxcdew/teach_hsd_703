var LabelMenu = Angel.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,labelname,normalImage, callback, back) {
		this._super(parent, "#menuroot.png", callback, back);
		this.setHoverName("menu_bg.png");
		this.setRectByChild(true);
		this.check = false;
		this.loadlabel(labelname);

	},
	loadlabel:function(name){
		var label =new cc.LabelTTF(name,gg.fontName,gg.menufont);
		label.setAnchorPoint(0,0.5);
		label.setColor(cc.color(0,0,0, 250));
		label.setPosition(60,this.height/2);
		this.addChild(label);
	},

	preCall:function(){
		cc.log("正确");
	},
	exeUnEnable:function(){
		cc.log("错误");
	},
	left:function (standard, margin){
		margin = 180;
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的x - 标准物的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * (1-锚点x) - 所需间隔
		var x = standard.x - standard.width * standard.getScaleX() * sap.x  - this.width * (1-ap.x) * this.getScaleX() - this.margin;
		this.setPosition(x, standard.y);
	},	
	right:function (standard, margin){
		margin = 180;
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 左边的x + 左边的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * 锚点
		var x = standard.x + standard.width * standard.getScaleX() * sap.x  + this.width * ap.x * this.getScaleX() + this.margin;
		this.setPosition(x, standard.y);
	},
	down:function (standard, margin){
		margin = 201;
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
		var y = standard.y - standard.height * standard.getScaleY() * sap.y  - this.height * (1-ap.y) * this.getScaleY() - this.margin;
		this.setPosition(standard.x, y);
	},	
})