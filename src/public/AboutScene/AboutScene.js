var AboutLayer = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function (p) {
		this._super();
		p.addChild(this);
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_public_about.about_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new AboutBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new AboutMainLayer(this);
	}
});

var AboutScene = PScene.extend({
	url: null,
	ctor:function(url){
		this._super();
		this.url = url;
	},
	onEnter:function () {
		this._super();
		var layer = new AboutLayer(this);
	}
});
