var ShowBackgroundLayer = cc.Layer.extend({
    ctor:function (tag) {
        this._super();
        this.loadBg();
        this.loadback();
        this.loadtitle(tag);
        this.loadNextButton();
        return true;
    },
    loadBg : function(){
    	var bglayer = new cc.LayerColor(cc.color(249, 245, 220, 250),1280,768);
    	bglayer.setPosition(0 , 0);
    	this.addChild(bglayer);
    },
    loadback :function(){
    	this.backButton = new ButtonScale(this,"#button/show_back.png", this.callback);
    	this.backButton.setAnchorPoint(0,0.5);
    	this.backButton.setLocalZOrder(5);
    	this.backButton.setPosition(cc.p(40, gg.height -this.backButton.height * 0.5 - 40));
    	this.backButton.setTag(TAG_BACK);
    },
    loadtitle :function(tag){
    	for(var i in expInfo){
    		var exp = expInfo[i];
    		if(gg.expId == exp.expId){
    			for(var j in exp.menuItem ){
    				menuItem = exp.menuItem[j];
    				if(menuItem.tag == tag){
    					var  label = new cc.LabelTTF(menuItem.menu,gg.fontName,30);
    					label.setAnchorPoint(0,0.5);
    					label.setPosition(152,this.backButton.y);
    					//label.setColor(cc.color(19, 98, 27, 250));
    					label.setColor(cc.color(0, 0, 0, 250));
    					this.addChild(label,5);
    					break;
    				}
    			}
    			break;
    		}  		
    	}
    },
    loadNextButton:function(){	
    	for(var i =1;i < expInfo.length; i++){
    		var exp =expInfo[i];
    		if(exp.expTag == gg.runExpTag){
    			for(var j =0; j< gg.menuArr.length ;j++){
    				var menu = gg.menuArr[j];
    				if(menu.tag == gg.runMenuTag){
    					if(j < gg.menuArr.length -1){//不是最后一个menu
    						var label = new Label(this,gg.menuArr[j+1].menu+">>",this.nextMenu);
    						label.setLocalZOrder(5);
    						label.setTag(gg.menuArr[j+1].tag);
    						label.setAnchorPoint(1, 0);
    						label.setColor(cc.color(19, 98, 27, 250));
    						label.setPosition(gg.width - 40,25);

    					}else{//如果是最后一个menu，则跳到下一节
    						if(!!expInfo[i+1]){//如果不是最后一节
    							var label = new Label(this,expInfo[i+1].expName+">>",this.nextChapter);
    							label.setLocalZOrder(5);
    							label.setTag(expInfo[i+1].expTag);  
    							label.setAnchorPoint(1, 0);
    							label.setColor(cc.color(19, 98, 27, 250));
    							label.setPosition(gg.width - 40,25);
    						}   						
    					}
    					
    					break;
    				}

    			}
    			break;
    		}
    	}
    },
    callback:function(){
    	ch.gotoStart(gg.runExpTag, g_resources_public_start, res_public_start.start_p );//返回开始界面	
    },
    nextMenu:function(p){
    	gg.runNext(p.getTag());
    },
    nextChapter:function(p){
    	ch.run(p.getTag());
    },
});