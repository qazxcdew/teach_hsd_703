var SelectMainLayer = cc.Layer.extend({
	is_runing:false,//是否正在滑动
	firstx:null,//初始button1的横坐标
	lastx:null,//初始最后录一个button在遮罩中的横坐标
    ctor:function () {
        this._super();
        // 加载菜单栏
        this.init();        
        return true;
    },
    init: function () { 
    	this.expbutonarr = [];
    	this.boundarr = [];
    	//增加遮罩
    	this.stencil = new cc.DrawNode()
    	//模板范围，左下角坐标，右上角坐标，构成的矩形，默认可见true
    	this.stencil.drawRect(cc.p(180,195),cc.p(1100,479),cc.color(248,232,176,100),1,cc.color(248,232,176,100));
    	this.rect = new cc.rect(180,195,1100-180,479-195);
    	var clipping = new cc.ClippingNode(this.stencil);
    	this.addChild(clipping, 20,TAG_CLIPPING);
    	
    	this.node = new cc.Node();
    	clipping.addChild(this.node);	    	
    	var length = expInfo.length-1;
    	var pagescount = Math.floor(length/4);
    	var page = length%4;
    	
    	var m=1;    	
    	var posx=280;//左边180留空，加本身宽度一半（width= 200）
    	var posy=337;//下边195留空，加本身高度一半（height = 284）   	    	
    	for (var i=0;i<length;i++){
    		var expbutton= new ButtonScale(this.node,expInfo[m].expimage,this.callback,this);
    		expbutton.x = posx;
    		expbutton.y = posy;
    		expbutton.setTag(expInfo[m].expTag);
    		m=m+1;
    		posx=posx+240;
    		this.expbutonarr[i] = expbutton;
    		this.boundarr[i] = $.genBoundingBoxToWorld(expbutton);
    		
    	}		
    	firstx = this.boundarr[0].x + this.expbutonarr[0].width /2;
    	if(page == 0){
    		lastx = firstx + 720;    		
    	}else if(page == 1){
    		lastx = firstx;   		
    	}else if(page == 2){
    		lastx = firstx + 240;   		
    	}else{
    		lastx = firstx + 480;
    		
    	}
		if('keyboard' in cc.sys.capabilities){
			cc.eventManager.addListener({  
				event : cc.EventListener.KEYBOARD,          // 键盘监听  
				onKeyPressed : this.onKeyPressed.bind(this), 
				onKeyReleased : this.onKeyReleased.bind(this)  
			}, this);  
		}
    	this.turnmenu();
    	this.loadSlide();
    	this.doupdate();
		return true;
    	
    },
    turnmenu:function(){
    	var winSize = cc.winSize; 
    	this.turnleft = new ButtonScale(this,res_public.leftbutton,this.turnarrow);
    	this.turnleft.setPosition(80,376-50);
    	this.turnleft.setVisible(false);       
    	this.turnright = new ButtonScale(this,res_public.rightbutton,this.turnarrow);
    	this.turnright.setPosition(1200,376-50);
    	if(expInfo.length <= 5){
    		this.turnright.setVisible(false);
    	}        
    },
    doupdate:function(){  	
    	max = this.boundarr.length;
    	for(var i = 0;i< max ; i++){
    		this.boundarr[i] = $.genBoundingBoxToWorld(this.expbutonarr[i]);   		
    	}
    	this.firstposx = this.boundarr[0].x + this.expbutonarr[0].width /2 ;//第一个button的中心点横坐标
    	//最后一个button的中心点横坐标
    	this.lastposx = this.boundarr[this.boundarr.length -1].x + this.expbutonarr[this.expbutonarr.length -1].width /2;
    	//如果最后一个按钮在遮罩内，向右按钮不可见
    	if( this.lastposx>180 && this.lastposx <1100){
    		this.turnright.setVisible(false);
    	}else{
    		this.turnright.setVisible(true);
    	}   	
    	//如果最后一个按钮在遮罩内，向左按钮不可见
    	if(this.firstposx >180 && this.firstposx <1100){
    		this.turnleft.setVisible(false);
    	}else{
    		this.turnleft.setVisible(true);
    	}  	
    },
    click:function(obj){//左右滑动，按钮进行缩放
    	obj.setColor(cc.color(92, 92, 92)); 
    	var seq = cc.sequence(cc.scaleTo(0.05, 0.85),cc.scaleTo(0.05, 0.75),
    			cc.delayTime(0.05),cc.scaleTo(0.05, 0.9),cc.scaleTo(0.1, 1),cc.callFunc(function(){
    				obj.setColor(cc.color(255, 255, 255));   		
    			},this));
    	obj.runAction(seq);
    },
    turnarrow:function(p){
    	switch(p){
    	case this.turnleft:    		
    		this.right();
    		break;
    	case this.turnright:     		
    		this.left();
    		break;
    	}
    	
    },
    left:function (){//button左滑
    	if( (lastx - 5)<this.lastposx && this.lastposx <(lastx + 5) ||this.is_runing){
    		return;
    	}
    	this.is_runing = true;
    	var seq = cc.sequence(cc.moveBy(0.5,cc.p(-960, 0)),cc.callFunc(function(){
    		this.doupdate();
    		this.is_runing = false;
    	}, this));
    	this.node.runAction(seq);
    },
    right:function (){//button右滑
    	if( this.firstposx >= (firstx - 5) ||this.is_runing){
    		return;
    	}
    	this.is_runing = true;
    	var seq = cc.sequence(cc.moveBy(0.5,cc.p(960, 0)),cc.callFunc(function(){
    		this.doupdate();
    		this.is_runing = false;
    	}, this));
    	this.node.runAction(seq);
    },
    callback:function(p){
    	//是否正在滑动
    	if(this.is_runing){
           return;
    	}
    	var bound =  $.genBoundingBoxToWorld(p);  	
    	if((bound.x + p.width/2) >180 && (bound.x + p.width/2) <1100){//是否在遮罩范围内，， 
    		ch.run(p.getTag());
    	}  	  	
    },
    onKeyPressed : function(key, event) {  
    	// android设备上  引擎可能貌似无法处理按下操作  
    }, 
    onKeyReleased : function(key, event) {  
    	// 所有逻辑在弹起时做  
    	//cc.log("key:" + key);  
    	switch (key) {  
    	// android TV: 左:159  右:160 上:161 下:162 OK:163 MENU:18  BACK:6  
    	case cc.KEY.left:   // 上  android:161 win32:28  
    		this.click(this.turnleft);
    		this.right();   		
    		break;  
    	case cc.KEY.right:  
    		this.click(this.turnright);
    		this.left();  
    		break;  
    	default:  
    		break;  
    	}  
    },  
    loadSlide:/**
     * 滑动
     */
    	function(){
    	var listener_touch = cc.EventListener.create({
    		event: cc.EventListener.TOUCH_ONE_BY_ONE,
    		swallowTouches: false,
    		hover: false,
    		onTouchBegan:this.onTouchBegan,
    		onTouchMoved:this.onTouchMoved,
    		onTouchEnded:this.onTouchEnded});
    	cc.eventManager.addListener(listener_touch, this);
    	if ('mouse' in cc.sys.capabilities){
    		var listener_mouse = cc.EventListener.create({
    			event: cc.EventListener.MOUSE,
    			swallowTouches: false,
    			onMouseScroll:this.onMouseScroll
    		});
    		cc.eventManager.addListener(listener_mouse, this);
    	}
    },
    preX:0,
    onTouchBegan: function(touch, event){
    	var target = event.getCurrentTarget();
    	var pos = cc.p(touch.getLocationX(),touch.getLocationY());
    	if(cc.rectContainsPoint(
    			target.getBoundingBoxToWorld(),pos)){
    		this.click = true;
    		this.preX = pos.x;
    		return true;
    	}
    	return false;
    },
    onTouchMoved2: function(touch, event){
    	if(this.click){
    		var target = event.getCurrentTarget();
    		var pos = cc.p(touch.getLocationX(),touch.getLocationY());
    		if(this.last == null){
    			this.last = pos;
    		} else {
    			var m =pos.x - this.preX;
    			if(m > 240 || m < -240){
    				var node = target.node;    				
    				node.runAction(cc.moveBy(0.5, cc.p(m, 0)));
    				this.preX = pos.x;
    			}   			
    		}
    	}
    },
    onTouchMoved: function(touch, event){
    	if(this.click){
    		var target = event.getCurrentTarget();
    		var pos = cc.p(touch.getLocationX(),touch.getLocationY());
    		if(this.last == null){
    			this.last = pos;
    		} else {
    			var margin = pos.x - this.last.x;//滑动距离  以一个button的宽度为界限  			
    			if(cc.rectContainsPoint(new cc.rect(180,195,1100-180,479-195),pos)){//判断滑动点是否在遮罩内
    				if(margin >= 200 && margin > 0){   
    					target.click(target.turnleft);
    					target.right();    				
    					this.last = pos;
    				} else if( margin < -200){
    					target.click(target.turnright);
    					target.left();
    					this.last = pos;
    				}   				
    			}   			
    		}
    	}
    },
    onTouchEnded: function(touch, event){
    	if(this.click){
    		this.click = false;
    		this.last = null;
    	}
    },
    onMouseScroll: function(event){//选择界面的滚动混轮
    	var target = event.getCurrentTarget();
    	if(event.getScrollY() > 0){//上滚
    		if(cc.sys.isNative
    				&& cc.sys.os == "Windows"){
    			target.click(target.turnright);
    			target.left();
    		} else {
    			target.click(target.turnleft);
    			target.right();
    		}						
    	} else if(event.getScrollY() < 0){//下滚
    		if(cc.sys.isNative
    				&& cc.sys.os == "Windows"){
    			target.click(target.turnleft);
    			target.right();
    		} else {
    			target.click(target.turnright);
    			target.left();	
    		}					
    	}
    }
    
});
