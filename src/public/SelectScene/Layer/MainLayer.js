var SelectMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	pageView:null,
	turnleft:null,
	turnright:null,
	curIndex: 0,
    ctor:function () {
        this._super();
        // 加载菜单栏
        this.init(); 
        this.turnmenu();
        this.scheduleUpdate();
        this.init1();
        return true;
    },
    init: function () {
    	var winSize = cc.winSize; 
    	this.pageView = new ccui.PageView();
    //	this.pageView = new MyPageView();
    	if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
    		// 浏览器 		
    	} else if(cc.sys.os == "Android"|| cc.sys.os == "IOS"|| cc.sys.os == "iOS"){
    		// Android IOS
    		this.pageView.setCustomScrollThreshold(50); //设置滑动的阀值
    	} else if(cc.sys.os == "Windows"){
    	}   	
    	this.pageView.setSwallowTouches(false);
    	this.pageView.setContentSize(cc.size(960, 284));
    	this.pageView.setPosition(winSize.width/2,winSize.height/2-50);
    	this.pageView.setAnchorPoint(0.5,0.5);
    	
    	var length = expInfo.length-1;
    	var m=1;
    	var pages =Math.floor(length/4);
    	var page = length%4;
    	    	
		for (var i = 0; i < pages; ++i) {
			var layout = new ccui.Layout();
			layout.setContentSize(cc.size(960, 284));
			var layoutRect = layout.getContentSize();
	        var posx=120;
	        var posy=layoutRect.height / 2;
			for (var j=0;j<4;j++){
				var imageView= new ButtonScale(layout,expInfo[m].expimage,this.callback,this);
				imageView.x = posx;
				imageView.y = posy;
				imageView.setTag(expInfo[m].expTag);
				m=m+1;
				posx=posx+240;
			}	
			this.pageView.addPage(layout);
		}
		if(page !=0 ) {	
			var layout = new ccui.Layout();
			layout.setContentSize(cc.size(920, 284));
			var layoutRect = layout.getContentSize();
			var posx=120;
			var posy=layoutRect.height / 2;
			for (var j=0;j<page;j++){
				var imageView= new ButtonScale(layout,expInfo[m].expimage,this.callback,this);
				imageView.x = posx;
				imageView.y = posy;
				imageView.setTag(expInfo[m].expTag);
				m=m+1;
				posx=posx+240;
				
			}			
			this.pageView.addPage(layout);		
        }						
		this.pageView.addEventListener(this.pageViewEvent, this);
		this.addChild(this.pageView);
		if('keyboard' in cc.sys.capabilities){
			cc.eventManager.addListener({  
				event : cc.EventListener.KEYBOARD,          // 按键监听  
				onKeyPressed : this.onKeyPressed.bind(this),  
				onKeyReleased : this.onKeyReleased.bind(this)  
			}, this);  
		}
		return true;
    	
    },
    init1 :function(){
    	var pages=this.pageView.getPages().length;
    	for (var i = 1;i < pages;i++){
    		var lay = this.pageView.getPage(i);
    		for(var j = 0; j< lay.getChildrenCount();j++){
    			var bs= lay.getChildren()[j];
    			bs.setEnable(false);
    		} 
    	}	
    },
    turnmenu:function(){
    	var winSize = cc.winSize; 
    	this.turnleft = new ButtonScale(this,res_public.leftbutton,this.turnarrow);
        this.turnleft.setPosition(80,376-50);
        this.turnleft.setVisible(false);       
        this.turnright = new ButtonScale(this,res_public.rightbutton,this.turnarrow);
        this.turnright.setPosition(1200,376-50);   	    	
    },
    
    update:function(){
    	var index = this.pageView.getCurPageIndex();
    	if(this.curIndex != index && !this.pageView._isAutoScrolling){
    		this.exeIndex();
    	}
    },
    exeIndex:function(){
    	var index = this.pageView.getCurPageIndex();
    	var pages=this.pageView.getPages().length;
    	if(index < 1){
    		this.turnleft.setVisible(false);
    		this.turnright.setVisible(true);
    	}else if(index >pages-2){
    		this.turnleft.setVisible(true);
    		this.turnright.setVisible(false);
    	}else{
    		this.turnleft.setVisible(true);
    		this.turnright.setVisible(true);
    	}    	
    	for (var i = 0;i < pages;i++){
    		var lay = this.pageView.getPage(i);
    		for(var j = 0; j< lay.getChildrenCount();j++){
    			var bs= lay.getChildren()[j];
    			bs.setEnable(false);
    		} 
    	}
    	var lay = this.pageView.getPage(index);
    	for(var i = 0; i < lay.getChildrenCount();i++){
    		var bs= lay.getChildren()[i];
    		bs.setEnable(true);
    	}   	
    	this.curIndex = index;
    },
    pageViewEvent: function (sender, type) {
    	switch (type) {
    	case ccui.PageView.EVENT_TURNING:
    		var pageView = sender;
    		break;
    	default:
    		break;
    	}
    },
    callback:function(p){
    	ch.run(p.getTag());
    },
    turnarrow:function(p){
    	if(this.pageView._isAutoScrolling){
    		return;
    	}
    	var index = this.pageView.getCurPageIndex();
    	var pages=this.pageView.getPages().length;
     	var lay = this.pageView.getPage(index);
     	for(var i = 0; i < lay.getChildrenCount();i++){
     		var bs= lay.getChildren()[i];
     		bs.setEnable(false);
     	}
    	switch(p){
    	case this.turnleft:
    		index=index-1;
    		break;
    	case this.turnright:
    		index=index+1;
    		break;
    	}
    	this.pageView.scrollToPage(index);
    },
    down:function (standard, target, margin){
    	if(!margin){
    		margin = 0;
    	}
    	var sap = standard.getAnchorPoint();
    	var ap = target.getAnchorPoint();
    	// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
    	var y = standard.y
    		- standard.height * standard.getScaleY() * sap.y
    		- target.height * (1-ap.y) * target.getScaleY() - margin;
    	target.setPosition(standard.x, y);
    },
    onKeyPressed : function(key, event) {  
    	// android设备上  引擎可能貌似无法处理按下操作  
    },  
    onKeyReleased : function(key, event) {  
    	// 所有逻辑在弹起时做  
    	//cc.log("key:" + key);  
    	switch (key) {  
    	// android TV: 左:159  右:160 上:161 下:162 OK:163 MENU:18  BACK:6  
    	case cc.KEY.left:   // 上  android:161 win32:28  
    		this.pageView.scrollToPage(this.pageView.getCurPageIndex()-1);   		
    		break;  
    	case cc.KEY.right:  
    		this.pageView.scrollToPage(this.pageView.getCurPageIndex()+1);  
    		break;  
    	default:  
    		break;  
    	}  
    }  
});
