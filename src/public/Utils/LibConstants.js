TAG_LIB_MIN = 30000;

TAG_CANCEL=30001;
TAG_SURE=30002;
TAG_WORD_BG = 30003;
TAG_LABEL = 30004;
TAG_LIB_TIP_CLOSE = 30005;

TAG_LIB_BG=30010;
TAG_LIB_BG1=30011;
TAG_LIB_BG2=30012;
TAG_LIB_BG3=30013;
TAG_LIB_BG4=30014;
TAG_LIB_BG5=30015;
TAG_LIB_BG6=30016;
TAG_LIB_BG7=30017;
TAG_LIB_BG8=30018;
TAG_LIB_BG9=30019;
TAG_LIB_BG10=30020;


TAG_LIB_AMPULLA = 30051;
TAG_LIB_BEAKER=30052;
TAG_LIB_CONICAL = 30053;
TAG_LIB_TESTTUBE = 30054;
TGA_LIB_GLASS = 30055;
TAG_LIB_INOCULATOR = 30056;
TAG_LIB_FLASK = 30057;
TAG_LIB_LAMP = 30058;
TAG_LIB_CELIJI = 30059;
TAG_LIB_YIBEI = 30060;    

TAG_LIB_NACL = 30061;
TAG_LIB_BALANCE = 30062;
TAG_LIB_CYLINDER = 30063;
TAG_LIB_WATER = 30064;

TAG_LIB_GUOLV =30065;
TAG_LIB_ZHENGFA =30066;
TAG_LIB_ROD =30067;
TAG_LIB_SALT = 30068;

TAG_LIB_DAOXIAN = 30069;
TAG_LIB_DIANJIEQI = 30070;
TAG_LIB_DIYADIANYUAN = 30071;
TAG_LIB_MATCH = 30072;


TAG_LIB_IRON1 = 30073;
TAG_LIB_LENGNING = 30074;
TAG_LIB_ZHENGLIU= 30075;

TAG_LIB_IRON2 = 30076;
TAG_LIB_DUILIU = 30077;
TAG_LIB_XIANXIANG = 30078;

libRelArr = [
             {tag:TAG_LIB_MIN, name:""},  
             {tag:TAG_LIB_AMPULLA, name:"集气瓶",img:"#apparatus/ampulla.png"},
             {tag:TAG_LIB_BEAKER,name:"烧杯",img:"#apparatus/beaker.png"},        
             {tag:TAG_LIB_CONICAL,name:"锥形瓶",img:"#apparatus/conical.png"},      
             {tag:TAG_LIB_TESTTUBE,name:"试管",img:"#apparatus/testTube.png"},                   
             {tag:TGA_LIB_GLASS,name:"玻璃片",img:"#apparatus/libglass.png"},
             {tag:TAG_LIB_INOCULATOR,name:"注射器",img:"#apparatus/libinoculator.png"},                        
             {tag:TAG_LIB_FLASK,name:"烧瓶",img:"#apparatus/libflask.png"},
             {tag:TAG_LIB_LAMP,name:"酒精灯",img:"#apparatus/liblamp.png"},
             {tag:TAG_LIB_CELIJI,name:"弹簧测力计",img:"#apparatus/libyaliji.png"},
             {tag:TAG_LIB_YIBEI,name:"溢水杯",img:"#apparatus/libyibei.png"},
             
             {tag:TAG_LIB_NACL,name:"食盐",img:"#apparatus/libnacl.png"},
             {tag:TAG_LIB_BALANCE,name:"托盘天平",img:"#apparatus/libbalance.png"},
             {tag:TAG_LIB_CYLINDER,name:"量筒",img:"#apparatus/libcylinder.png"},
             {tag:TAG_LIB_WATER,name:"水壶",img:"#apparatus/libwater.png"},
             
             {tag:TAG_LIB_GUOLV,name:"过滤装置",img:"#apparatus/libguolv.png"},
             {tag:TAG_LIB_ZHENGFA,name:"蒸发装置",img:"#apparatus/libzhengfa.png"},
             {tag:TAG_LIB_ROD,name:"玻璃棒",img:"#apparatus/librod.png"},
             {tag:TAG_LIB_SALT,name:"粗食盐",img:"#apparatus/libSalt.png"},

             {tag:TAG_LIB_DAOXIAN,name:"导线",img:"#apparatus/daoxian.png"},
             {tag:TAG_LIB_DIANJIEQI,name:"水电解器",img:"#apparatus/libelectrolysers.png"},
             {tag:TAG_LIB_DIYADIANYUAN,name:"低压直流电源",img:"#apparatus/libdiyazhiliu.png"},
             {tag:TAG_LIB_MATCH,name:"火柴",img:"#apparatus/libmatch.png"},
             
             {tag:TAG_LIB_IRON1,name:"铁架台",img:"#apparatus/libiron.png"},
             {tag:TAG_LIB_LENGNING,name:"冷凝管",img:"#apparatus/liblengning.png"},
             {tag:TAG_LIB_ZHENGLIU,name:"蒸馏装置",img:"#apparatus/libzhengliu.png"},

             {tag:TAG_LIB_IRON2,name:"铁架台",img:"#apparatus/libiron1.png"},
             
             {tag:TAG_LIB_DUILIU,name:"对流装置",img:"#apparatus/libduiliu.png"},
             {tag:TAG_LIB_XIANXIANG,name:"线香",img:"#apparatus/libxianxiang.png"},
             
             ];
