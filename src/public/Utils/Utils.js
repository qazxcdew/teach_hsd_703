var $ = {
		checkNull:/**
		 * 检查是否为空
		 * 
		 * @param str
		 * @returns {Boolean}
		 */
			function(str){
			if(str == null || str == ""){
				return true;
			}
			return false;
		},
		sdf:function(date, format){
			return date.format("yyyyMMddhhmmss"); 
		},
		getQueryString:function(name) {
		//	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
			var reg = new RegExp("(^|&)" + name + "=([^&|/]*)(&|$|/)", "i");
			var r = window.location.search.substr(1).match(reg);
			if (r != null){
				var result =  decodeURIComponent(r[2]);	
				return result;
			}
			return null;
		},
		getRandom:/**
		 * 取得随机数
		 * 
		 * @param max
		 * @returns {Number}
		 */
			function(max){
			var random = Math.round(Math.random() * max);
			if(random == max){
				random = max - 1;
			}
			return random;
		},
		format:/**
		 * 自动换行
		 * 
		 * @param str
		 * @param width
		 * @param fontSize
		 * @returns
		 */
			function(str, width, fontSize){
//			if(str.split("\n").length >1){
//				return str;
//			}
			// 每行字数
			var size = parseInt(width / fontSize, 10);
			var lines = "";
			var count = 0;
			var cCount = 0;
			var isChinese = false;
			for(var i = 0; i< str.length; i++){
				lines += str[i];
				if(str[i] == "\n"){
					count = 0;
					continue;
				}
				isChinese = this.isChinese(str[i]);
				if(!isChinese){
					cCount ++;//单字节字符加1
					if(cCount >= 2){
						cCount = 0;
						count ++;//双字节字符加1
						isChinese = false;
						if(count % size == 0  && str.length > 0){
							lines += "\n";
						}	
					}								
					continue;
				}
				count ++;//双字节字符加1
				if(count % size == 0  && str.length > 0){
					lines += "\n";
				}
				if(cc.sys.os == "Windows"
					&& str[i] == 63){
					isChinese = true;
					//cc.log("isChinese||" + isChinese);
				}
			}
			if(str.length %size == 0  && str.length >= size){
				lines = lines.substr(0, lines.length-1)//截取从0开始到倒数第二个的字符串，作用是去掉最后一个换行
			}
			return lines;
		},
		isChinese:function(str){
			//cc.log("isChinese:str||" + str);
			var text = " ";
			//var reg = /^[\u4E00-\u9FA5]+$/; 
			var reg = /^[^\x00-\xff]+$/; //双字节的正则匹配
		    if(reg.test(str)){
				//cc.log("isChinese:result||true");
				return true;   
			} else {
				//cc.log("isChinese:result||false");
				return false;
			}
		},
		genBoundingBoxToWorld:/**
		 * 生成相对整个的bound，
		 * 区别于Node.genBoundingBoxToWorld，不涉及子节点
		 * 
		 * @param obj
		 * @returns
		 */
			function(obj){
			var rect = cc.rect(0, 0, obj.width, obj.height);
			var trans = obj.getNodeToWorldTransform();
			var bound = cc.rectApplyAffineTransform(rect, trans);
			return bound;
		},
		runScene:function(scene){
			cc.director.runScene(new cc.TransitionFade(gg.d_time, scene));
			AngelListener.regListener(scene);
			ButtonScaleListener.regListener(scene);
		},
		pushScene:function(scene){
			cc.director.pushScene(new cc.TransitionFade(gg.d_time, scene));
			AngelListener.regListener(scene);
			ButtonScaleListener.regListener(scene);
		},
		randomSort:function (a, b) {
			// 用Math.random()函数生成0~1之间的随机数与0.5比较，返回-1或1
			return Math.random()>.5 ? -1 : 1;
		},
		bezier:function(orig,target,time){
			var pos2 = cc.p(orig.x - (orig.x - target.x) / 2,orig.y - (orig.y - target.y) / 3);
			var pos1 = cc.p(orig.x - (orig.x - target.x) / 3,orig.y - (orig.y - target.y) / 2);
//			var pos1 = cc.p((orig.x + target.x) / 3, (orig.y + target.y) / 3);
//			var pos2 = cc.p((orig.x + target.x) / 2, (orig.y + target.y) / 2);
			var pos3 = target;
			return cc.bezierTo(time, [pos1, pos2, pos3]);
		},
		calWidthAndHeight:function(designWidth, designHeight){
			// define the design resolution
//			var designWidth = 960;
//			var designHeight = 640;
			// retrieve device resolution
			var deviceWidth = cc.visibleRect.width;
			var deviceHeight = cc.visibleRect.height;
			var k = 1, x = 0, y = 0;

			k = deviceWidth / designWidth;
			var scaledHeight = designHeight * k;
			if (scaledHeight <= deviceHeight) {
				y = (deviceHeight - scaledHeight);
			} else {
				k = deviceHeight / designHeight;
				var scaledWidth = designWidth * k;
				if (scaledWidth <= deviceWidth) {
					x = (deviceWidth - scaledWidth);
				} else {
					throw new Error("can't fit the screen!");
				}
			}
			// print out parameters
			cc.log("device width:" + deviceWidth);
			cc.log("device height:" + deviceHeight);
			cc.log("k:" + k + " x:" + x + " y:" + y);
			// resize the design resolution
			cc.view.setDesignResolutionSize(
					designWidth + x / k, designHeight + y / k,
					cc.ResolutionPolicy.SHOW_ALL);
			// after screen fitted
			cc.log("view width:" + cc.visibleRect.width + " view height:" + cc.visibleRect.height);
		},
		sayTeachFlow:function(){
			for(var i in teachFlow){
				var flow = teachFlow[i];
				cc.log(flow.tip);
			}
		},
		up:function (standard, target, margin){
			if(!margin){
				margin = 0;
			}
			var sap = standard.getAnchorPoint();
			var ap = target.getAnchorPoint();
			// 标准物的y + 标准物的高度 * 缩放 * (1-锚点y) + 本身的高度 * 缩放 * 锚点y + 所需间隔
			var y = standard.y + standard.height * standard.getScaleY() * (1-sap.y)  + target.height * ap.y * target.getScaleY() + margin;
			target.setPosition(standard.x, y);
		},	
		down:function (standard, target, margin){
			if(!margin){
				margin = 0;
			}
			var sap = standard.getAnchorPoint();
			var ap = target.getAnchorPoint();
			// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
			var y = standard.y - standard.height * standard.getScaleY() * sap.y  - target.height * (1-ap.y) * target.getScaleY() - margin;
			target.setPosition(standard.x, y);
		},
		left:function (standard, margin){
			if(margin != null){
				this.margin = margin;
			}
			var sap = standard.getAnchorPoint();
			var ap = this.getAnchorPoint();
			// 标准物的x - 标准物的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * (1-锚点x) - 所需间隔
			var x = standard.x - standard.width * standard.getScaleX() * sap.x  - this.width * (1-ap.x) * this.getScaleX() - this.margin;
			this.setPosition(x, standard.y);
		},	
		right:function (standard, margin){
			if(margin != null){
				this.margin = margin;
			}
			var sap = standard.getAnchorPoint();
			var ap = this.getAnchorPoint();
			// 标准物的x + 标准物的宽度 * 缩放 * (1-锚点x) + 本身的宽度 * 缩放 * 锚点 + 需要间隔
			var x = standard.x + standard.width * standard.getScaleX() * (1-sap.x)  + this.width * ap.x * this.getScaleX() + this.margin;
			this.setPosition(x, standard.y);
		},
		isCanvs:/**
		 * 判断是否画板渲染
		 * 
		 * @returns {Boolean}
		 */
			function(){
			if(cc.sys.platform === cc.sys.DESKTOP_BROWSER){
				if(cc._renderType === cc._RENDER_TYPE_CANVAS){
					return true
				} else {
					return false;
				}
			} else {
				// 不是浏览器,一定不是canvas
				return false;
			}
		}
};

Date.prototype.format = function(format){ 
	var o = { 
			"M+" : this.getMonth()+1, // month
			"d+" : this.getDate(), // day
			"h+" : this.getHours(), // hour
			"m+" : this.getMinutes(), // minute
			"s+" : this.getSeconds(), // second
			"q+" : Math.floor((this.getMonth()+3)/3), // quarter
			"S" : this.getMilliseconds() // millisecond
	} 
	if(/(y+)/.test(format)) { 
		format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
	} 
	for(var k in o) { 
		if(new RegExp("("+ k +")").test(format)) { 
			format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
		} 
	} 
	return format; 
}

