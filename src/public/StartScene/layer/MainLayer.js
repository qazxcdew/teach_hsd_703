TAG_YINDAO = 1001;
TAG_SHIZHAN = 1002;
TAG_QUWEI = 1003;
TAG_QITA = 1004;
TAG_ABOUT = 1401;
TAG_TEST = 1402;
TAG_LIANX = 1403;
TAG_YUANL = 1404;
TAG_MUDE = 1405;
TAG_DTL = 1406;
TAG_PLATFORM = 1407;
TAG_WIN=1408;

var StartMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	title:null,
	YuanLi:null,
	MuDi:null,
	chapterImg:null,
	ctor:function () {
		this._super();
		// 加载回退按钮
		this.loadButton();
		//获取配置信息
		this.loadInfo();
		//加载章节
		this.loadChapterImg();
		// 加载标题
		this.loadTitle();
		//加载左侧图片
		this.loadimg();
		//右侧菜单
		this.loadMenu();
		//添加滚动
		this.loadSlide();
		return true;
	},
	loadInfo:function(){//获取实验配置信息
		gg.menuArr = [];
		for(var i in expInfo){
			var exp = expInfo[i];
			if(gg.expId == exp.expId){
				this.chapterImg = exp.chapterImg;
//				this.title = exp.expName;
				this.startImg = exp.startPng;				
				for(j=0;j<exp.menuItem.length;j++){
					gg.menuArr.push(exp.menuItem[j]);
				}
				break;
			}		  
		}		
	},
	loadChapterImg: function(){//标题
		var chapterImg = new cc.Sprite(this.chapterImg);
		chapterImg.setPosition(395,gg.height - chapterImg.height/2 - 50);
		this.addChild(chapterImg,5);
	},
	loadTitle: function(){//标题
		var title = new cc.LabelTTF(gg.Title,gg.fontName,45);
		title.setPosition(395,gg.height - title.height/2 -180);
		this.addChild(title,5);
	},
	loadimg:function(){//左侧图片
		var startImg = new cc.Sprite(this.startImg);
		startImg.setPosition(395,gg.height - startImg.height/2 -360);
		this.addChild(startImg,5);
		
	},
	loadButton:function(){//返回按钮
		this.backButton = new ButtonScale(this,"#button/button_back.png", this.callback);
		this.backButton.setLocalZOrder(5);
		this.backButton.setPosition(cc.p(20 + this.backButton.width * 0.5, gg.height - 20 - this.backButton.height * 0.5));
		this.backButton.setTag(TAG_BACK);
	},
	loadMenu :function(){//右侧菜单
		var bglayer = new cc.LayerColor(cc.color(255, 255, 255, 250),490,768);
		bglayer.setPosition(790 , 0);
		this.addChild(bglayer);
		
		this.node = new cc.Node();
		this.node.setPosition(0,0);
		var prev = null;
		this.cur = 0;
		var marign = 0;
		
		for(i=0;i<gg.menuArr.length;i++){

			var label = new LabelMenu(this.node,gg.menuArr[i].menu,"#menu_bg.png",this.callback,this);
			label.setAnchorPoint(0, 0.5);
			label.setTag(gg.menuArr[i].tag);				
			if(!prev){
				label.setPosition(790,664);
			} else {
				this.posDown(prev, label, marign);
			}
			prev = label;
			if(!this.cell){ 
				this.cell = label.height + marign;
			}				
		}
//		for(var i in expInfo){
//			var exp = expInfo[i];
//			if(gg.expId == exp.expId){
//				this.menulength = exp.menuItem.length;
//				for(i=0;i<exp.menuItem.length;i++){
//					var label = new cc.MenuItemLabel(
//							new cc.LabelTTF(exp.menuItem[i].menu,gg.fontName,gg.menufont),
//							this.callback,this);
//					label.setAnchorPoint(0, 0.5);
//					label.setColor(cc.color(255,255,255, 200));
//					label.setTag(exp.menuItem[i].tag);
//					this.menu.addChild(label, 10);				
//					if(!prev){
//						label.setPosition(680,520);
//					} else {
//						this.posDown(prev, label, marign);
//					}
//					prev = label;
//					if(!this.cell){ 
//						this.cell = label.height + marign;
//					}				
//				}
//				break;
//		    }								
//		}
		//添加遮罩
		var stencil = new cc.DrawNode();
		stencil.drawRect(cc.p(790,50),cc.p(1280,718),cc.color(248,232,176,0),0,cc.color(248,232,176,0));
		var clip = new cc.ClippingNode(stencil);
		this.addChild(clip,10);
		clip.addChild(this.node, 1);
	},
	posDown:function (standard, target, margin){
		if(!margin){
			margin = 0;
		}
		var sap = standard.getAnchorPoint();
		var ap = target.getAnchorPoint();
		// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
		var y = standard.y
		- standard.height * standard.getScaleY() * sap.y
		- target.height * (1-ap.y) * target.getScaleY() - margin;
		target.setPosition(standard.x, y);
	},
	callback:function(pSend){
		switch(pSend.getTag()){
		case TAG_BACK:
			cc.log("结束");
			if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
				ch.howToGo(OP_TYPE_BACK);
			} else if(cc.sys.os == "IOS"){
			} else if(cc.sys.os == "Android"
				|| cc.sys.os == "Windows"){
				ch.howToGo(OP_TYPE_BACK);
			}
			break;
		case TAG_MENU1:
		case TAG_MENU2:
		case TAG_MENU3:
		case TAG_MENU4:
		case TAG_MENU5:
		case TAG_MENU6:
			gg.runNext(pSend.getTag());
			break;
		}
	},
	down:/**
	 * 向下滑动
	 */
		function(){
		if(this.cur >= gg.menuArr.length - 6){
			return;
		}
		this.cur++;
		this.node.runAction(cc.moveBy(0.2,cc.p(0, this.cell)))
	},
	up:/**
	 * 向上滑动
	 */
		function(){
		if(this.cur <= 0){
			return;
		}
		this.cur--;
		this.node.runAction(cc.moveBy(0.2,cc.p(0, -this.cell)))
	},
	loadSlide:/**
	 * 滑动
	 */
		function(){
		var listener_touch = cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: false,
			hover: false,
			onTouchBegan:UpperLowerSliding.onTouchBegan,
			onTouchMoved:UpperLowerSliding.onTouchMoved,
			onTouchEnded:UpperLowerSliding.onTouchEnded});
		cc.eventManager.addListener(listener_touch, this);
		if ('mouse' in cc.sys.capabilities){
			var listener_mouse = cc.EventListener.create({
				event: cc.EventListener.MOUSE,
				swallowTouches: false,
				onMouseScroll:UpperLowerSliding.onMouseScroll
			});
			cc.eventManager.addListener(listener_mouse, this);
		}
	}
});
