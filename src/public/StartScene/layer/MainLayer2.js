TAG_YINDAO = 1001;
TAG_SHIZHAN = 1002;
TAG_QUWEI = 1003;
TAG_QITA = 1004;
TAG_ABOUT = 1401;
TAG_TEST = 1402;
TAG_LIANX = 1403;
TAG_YUANL = 1404;
TAG_MUDE = 1405;
TAG_DTL = 1406;
TAG_PLATFORM = 1407;
TAG_WIN=1408;

var StartMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	title:null,
	YuanLi:null,
	MuDi:null,
	ctor:function () {
		this._super();
		// 加载菜单栏按钮
		this.loadButton();
		//加载标题,实验目的,实验原理
		// 返回平台
		this.loadPlatform();
		// 加载标题
		this.loadTitle();
		return true;
	},
	loadTitle: function(){
		var title = new cc.LabelTTF(gg.Title ,"Arial","80");
		title.setPosition(395, gg.height - 266 - title.height * 0.5);
		this.addChild(title,2);
	},
	loadButton:function(){
		this.leadButton = new ButtonScale(this,"#button/YinD.png",this.callback);
		this.leadButton.setPosition(cc.p(gg.width - 125 - this.leadButton.width * 0.5, gg.height - 133 - this.leadButton.height * 0.5));
		this.leadButton.setLocalZOrder(5);
		this.leadButton.setTag(TAG_YINDAO);

		this.realButton = new ButtonScale(this,"#button/Shiz.png",this.callback);
		this.realButton.setLocalZOrder(5);
		this.realButton.setPosition(cc.p(gg.width - 125 - this.realButton.width * 0.5, gg.height - 275 - this.realButton.height * 0.5));
		this.realButton.setTag(TAG_SHIZHAN);

		this.purposeButton = new ButtonScale(this,"#button/MuDe.png",this.callback);
		this.purposeButton.setPosition(cc.p(gg.width - 125 - this.purposeButton.width * 0.5 , gg.height - 419 - this.purposeButton.height * 0.5));
		this.purposeButton.setLocalZOrder(5);
		this.purposeButton.setTag(TAG_MUDE);

		this.principleButton = new ButtonScale(this,"#button/YuanL.png",this.callback);
		this.principleButton.setPosition(cc.p(gg.width - 125 - this.principleButton.width * 0.5, gg.height - 561 - this.principleButton.height * 0.5));	
		this.principleButton.setLocalZOrder(5);
		this.principleButton.setTag(TAG_YUANL);


		this.aboutButton = new ButtonScale(this,"#button/about.png", this.callback);
		this.aboutButton.setLocalZOrder(5);
		this.aboutButton.setPosition(cc.p(20 + this.aboutButton.width * 0.5, gg.height - 20 - this.aboutButton.height * 0.5));
		this.aboutButton.setTag(TAG_ABOUT);

//		if(gg.expdemo ==TAG_TYPE_YANSHI ){//实验演示，自主演示
//			this.leadButton.setSpriteFrame("button/ExpDisP.png");
//			this.realButton.setSpriteFrame("button/DisSelf.png");
//		}else 
			if(gg.expdemo == TAG_TYPE_CESHI){//演示，测试
			this.leadButton.setSpriteFrame("button/yanshi.png");
			this.realButton.setSpriteFrame("button/ceshi.png");
		}

	},
	Win:function(str,str1){		
		var tipRunBg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
		this.addChild(tipRunBg,10, TAG_TIPRUNBG);
		tipRunBg.setOpacity(0);
		tipRunBg.runAction(cc.fadeTo(0.4, 200));

		var frame = new cc.Sprite(str);
		frame.setPosition(gg.c_width,gg.height);
		this.addChild(frame,10,TAG_WIN);		

		var label = new cc.LabelTTF(str1,"微软雅黑",36);
		label.setAnchorPoint(0,1);
		label.setPosition(cc.p(186,frame.height-213));		
		label.setColor(cc.color(137,64,0));	
		frame.addChild(label);

		this.close = new ButtonScale(frame,"#button/tip_frame_close.png",this.back,this);
		this.close.setPosition(1035, 600);

		var move = cc.moveTo(0.3,cc.p(gg.c_width,gg.c_height));
		var move2 = cc.moveTo(0.1,cc.p(gg.c_width,gg.c_height+50));
		var move3 = cc.moveTo(0.1,cc.p(gg.c_width,gg.c_height));
		var sequence = cc.sequence(move, move2, move3);
		frame.runAction(sequence);
		this.getChildByTag(TAG_YINDAO).setEnable(false);
		this.getChildByTag(TAG_SHIZHAN).setEnable(false);
		this.getChildByTag(TAG_YUANL).setEnable(false);
		this.getChildByTag(TAG_MUDE).setEnable(false);
		//this.getChildByTag(TAG_QUWEI).setEnable(false);
		this.getChildByTag(TAG_PLATFORM).setEnable(false);
		this.getChildByTag(TAG_ABOUT).setEnable(false);

	},
	cWin:function(name){
		if(name == null || name == ""){
			this.win.runAction(cc.fadeOut(0.15));	
			return;
		}
		var seq = cc.sequence(cc.fadeOut(0.15),cc.callFunc(function(){
			this.win.setSpriteFrame(name);
		}, this),cc.fadeIn(0.15));
		this.win.runAction(seq);
	},
	loadPlatform : function(){
		this.indexButton = new ButtonScale(this,"#button/platform.png", this.callback);
		this.indexButton.setLocalZOrder(5);
		this.indexButton.setPosition(cc.p(20 + this.indexButton.width * 0.5, 30 + this.indexButton.height * 0.5));
		this.indexButton.setTag(TAG_PLATFORM);
	},
	back:function(){
		var tipRunBg = this.getChildByTag(TAG_TIPRUNBG);
		var seq = cc.sequence(cc.fadeTo(0.4, 0),cc.callFunc(function(){
			tipRunBg.removeFromParent();
		},this));
		tipRunBg.runAction(seq);
		var win = this.getChildByTag(TAG_WIN);
		var move = cc.moveTo(0.3,cc.p(gg.c_width,gg.height+win.height/2));
		var sequence = cc.sequence(move,cc.callFunc(function(){
			win.removeFromParent();
		},this));
		win.runAction(sequence);

		this.getChildByTag(TAG_YINDAO).setEnable(true);
		this.getChildByTag(TAG_SHIZHAN).setEnable(true);
		this.getChildByTag(TAG_YUANL).setEnable(true);
		this.getChildByTag(TAG_MUDE).setEnable(true);
		//this.getChildByTag(TAG_QUWEI).setEnable(true);
		this.getChildByTag(TAG_PLATFORM).setEnable(true);
		this.getChildByTag(TAG_ABOUT).setEnable(true);
	},
	callback:function(pSend){
		switch(pSend.getTag()){
		case TAG_PLATFORM:
			cc.log("结束");
			if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
				ch.howToGo(OP_TYPE_BACK);
			} else if(cc.sys.os == "IOS"){
			} else if(cc.sys.os == "Android"
				|| cc.sys.os == "Windows"){
				//cc.director.end();	
				ch.howToGo(OP_TYPE_BACK);
			}
			break;
		case TAG_YINDAO:
			cc.log("进入引导模式");
			gg.teach_type = TAG_LEAD;
			gg.runNext(pSend.getTag());
			break;
		case TAG_SHIZHAN:
			cc.log(gg.expId);
			if(gg.expdemo == TAG_TYPE_CESHI)//如果是测试模式
			{
				gg.runNext(TAG_TEST);
			}
			else{
				cc.log("进入实战模式");
				gg.teach_type = TAG_REAL;
				gg.runNext(pSend.getTag());
			}
			break;
		case TAG_QUWEI:
			cc.log("进入小游戏模式");
			gg.runNext(pSend.getTag());
			break;
		case TAG_ABOUT:
			gg.runNext(pSend.getTag());
			break;
		case TAG_TEST:
			this.cWin("");
			gg.runNext(pSend.getTag());
			break;
		case TAG_LIANX:
			this.cWin("");
			break;
		case TAG_YUANL:
			this.Win(res_public.yuanli,gg.YuanLi);
			break;
		case TAG_MUDE:
			this.Win(res_public.mudi,gg.MuDi);
			break;
		}
	}

});
