var LoginMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
    ctor:function () {
        this._super();
        this.loadTip();
        this.loadEditBox();
        this.loadLoginButton();
        this.loadKey();
        return true;
    },
    loadKey:function(){
    	// 监听回车键
    	var self = this;
    	if ('keyboard' in cc.sys.capabilities){
    		var listener_key = cc.EventListener.create({
    			event: cc.EventListener.KEYBOARD,
    			onKeyReleased: function (key, event) {
    				cc.log( "Key up:" + key);
    				if(key == cc.KEY.enter){
    					self.postLogin();
    				}
    			}
    		});
    		cc.eventManager.addListener(listener_key, this);
    	}
    },
    loadTip:function(){
    	this._tipL = new cc.LabelTTF("", gg.fontName, 25);
    	this._tipL.setPosition(gg.c_width, gg.height * 0.62);
    	this._tipL.setColor(cc.color(5, 113, 189, 0));
    	this.addChild(this._tipL, 11);
    },
    loadEditBox:function (){
    	if(cc.sys.os == "Windows" && cc.sys.isNative){
    		var _workCode = new EditBox(this);
    		_workCode.setMaxLength(11);
    		_workCode.setHit("点击输入学号");
    		_workCode.setPos(392, gg.height * 0.55);
    		_workCode.setTag(TAG_XH);
    		_workCode.setLocalZOrder(gg.d_z_index);
    	} else {
    		var _workCode = new cc.EditBox(
    				cc.size(259, 40), 
    				new cc.Scale9Sprite("login_edit.png"));
    		_workCode.setPosition(gg.c_width,gg.height * 0.55);
    		_workCode.setPlaceHolder("点击输入学号"); 
    		_workCode.setFontColor(cc.color(0, 0, 0, 255));
    		_workCode.setMaxLength(11);
    		_workCode.setFontSize(30);
    		_workCode.setPlaceholderFontSize(25);
    		this.addChild(_workCode, gg.d_z_index, TAG_XH);
    	}
    	var account = new cc.Sprite("#account.png");
    	this.addChild(account, gg.d_z_index);
    	this.right(_workCode, account, 15);

    	if(cc.sys.os == "Windows" && cc.sys.isNative){
    		var _passwd = new EditBox(this);
    		_passwd.setMaxLength(11);
    		_passwd.setHit("点击输入密码");
    		_passwd.setPos(392, gg.height * 0.48);
    		_passwd.setTag(TAG_MM);
    		_passwd.setLocalZOrder(gg.d_z_index);
    		_passwd.setPasswordEnabled(true);
    	} else {
    		var _passwd = new cc.EditBox(
    				cc.size(259, 40), 
    				new cc.Scale9Sprite("login_edit.png"));
    		_passwd.setPosition(gg.c_width,gg.height * 0.48);
    		_passwd.setPlaceHolder("点击输入密码"); 
    		_passwd.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
    		_passwd.setFontColor(cc.color(0, 0, 0, 255));
    		_passwd.setFontSize(30);
    		_passwd.setPlaceholderFontSize(25);
    		this.addChild(_passwd, gg.d_z_index, TAG_MM);
    	}

    	var passwd = new cc.Sprite("#passwd.png");
    	this.addChild(passwd, gg.d_z_index);
    	this.right(_passwd, passwd, 15);

    	var userName = cc.sys.localStorage.getItem("userName");
    	var password = cc.sys.localStorage.getItem("password");
    	if(!!userName){
    		_workCode.setString(userName);
    		_passwd.setString(password);
    	}
    },
    loadLoginButton : function(){
    	var _loginItem = new cc.MenuItemImage(
    			"#finish_login.png","#finish_login.png",
    			this.eventMenuCallback,this);
    	_loginItem.setPosition(gg.c_width, gg.height * 0.4);

    	var menu = new cc.Menu();
    	menu.addChild(_loginItem, 1, TAG_LOGIN);
    	menu.setPosition(0, 0);
    	this.addChild(menu, 1);
    },
    login: function(url){
    	var xhr = cc.loader.getXMLHttpRequest();
    	xhr.open("GET", url);
    	result_layer = this;
    	xhr.onreadystatechange = function () {
    		if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status <= 207)) {
    			var result = xhr.responseText;
    			var jObject = JSON.parse(result);
    			login_layer.loginResult(jObject);
    		}
    	};
    	xhr.send();
    },
    logining:false,
    eventMenuCallback: function(pSender) {
    	if(this.logining){
    		return;
    	}
    	switch (pSender.getTag()){
	    	case TAG_LOGIN:
	    		this.postLogin();
	    		break;
	    	default:
	    		break;
    	}
    },
    postLogin:function(){
    	// 登录
    	var workCode = this.getChildByTag(TAG_XH).getString();
    	var passwd = this.getChildByTag(TAG_MM).getString();
    	if($.checkNull(workCode) || $.checkNull(passwd)){
    		this._tipL.setString("学号或密码不能为空！");
    		return;
    	}
    	else if(workCode==1&&passwd==1){
    		ch.howToGo(OP_TYPE_SELECT);
    		return;
    	}
    	cc.log("进入登录" + "学号" + workCode+ "    密码" + passwd);
    	this._tipL.setString("登录中...");
    	this.logining = true;
    	net.login(workCode, passwd, this.loginResult, this);
    },
    loginResult: function(json){
    	this.logining = false;
    	cc.log(json);
    	if(json == null || json.result == 1){
    		this._tipL.setString("用户名不存在或密码错误！");
    	} else {
    		uu.userId = json.object.userId;
    		ch.howToGo(OP_TYPE_SELECT);
    	}
    },
    right:function (standard, target, margin){
    	if(!margin){
    		margin = 5;
    	}
    	var sap = standard.getAnchorPoint();
    	var ap = target.getAnchorPoint();
    	// 标准物的x - 标准物的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * (1-锚点x) - 所需间隔
    	var x = standard.x - standard.width * standard.getScaleX() * sap.x  - target.width * (1-ap.x) * target.getScaleX() - margin;
    	target.setPosition(x, standard.y);
    }
});
