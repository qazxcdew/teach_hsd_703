exp03.Body = cc.Node.extend({
	status:null,
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BODY_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.handle= new Button(this, 12, TAG_HANDLE, "#handle.png",this.callback);	
		this.handle.setPosition(0,450);
		this.handle.setRotation(90);
				
		this.handle1 =  new cc.Sprite("#handle1.png");
    	this.handle1.setRotation(90);
    	this.handle1.setPosition(0,310);
		this.addChild(this.handle1);
		
		this.handle2 =  new cc.Sprite("#handle2.png");
		this.handle2.setRotation(90);
		this.handle2.setPosition(0,310);
		this.addChild(this.handle2,13)
			
		this.wipe =  new cc.Sprite("#rubber.png");
		this.wipe.setPosition(265,45);
		this.wipe.setRotation(-90);
		this.wipe.setScale(0.7,0.9);
		this.handle2.addChild(this.wipe);
		
		this.sai =  new cc.Sprite("#sai.png");
		this.sai.setPosition(10,125);
		this.sai.setScale(0.4,0.35);
		this.wipe.addChild(this.sai);
		
		this.bottle =  new cc.Sprite("#bottle.png");
		this.bottle.setPosition(145,0);
		this.bottle.setScale(1.1);
		this.sai.addChild(this.bottle);
		
		this.bottleline =  new cc.Sprite("#flumeline.png");
		this.bottleline.setPosition(145,30);
		this.bottleline.setScale(0.6);
		this.bottle.addChild(this.bottleline);
		

		
		this.flume2 =  new cc.Sprite("#flume2.png");
		this.flume2.setPosition(0,10);
		this.flume2.setScale(0.5,1.35);
		this.addChild(this.flume2);
		
		this.flume1 =  new cc.Sprite("#flume1.png");
		this.flume1.setPosition(0,10.4);
		this.flume1.setScale(0.5,1.35);
		this.addChild(this.flume1,20);
		
		this.flumeline =  new cc.Sprite("#flumeline.png");
		this.flumeline.setPosition(200,140);
		this.flume2.addChild(this.flumeline);
		
		
	},
	
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_HANDLE:
			if(action ==ACTION_DO1||this.status == 1){
				this.showtip = new ShowTip("玻璃瓶受到的浮力大小等于自身和" +
						"\n瓶内水的重力大小，玻璃瓶悬浮",cc.p(400,450));
				var move = cc.moveTo(1,cc.p(0,200));
				this.handle2.runAction(move);
				this.handle1.runAction(move.clone());
				
				var move1 = cc.moveTo(1,cc.p(200,180));
				this.flumeline.runAction(move1);
				
				var move2 = cc.moveTo(1,cc.p(0,260));
				var seq = cc.sequence(move2,cc.callFunc(function(){
					this.status = 2;
					this.flowNext(); 
					p.setEnable(true);
				},this));
				p.runAction(seq);	
				
				var move4 = cc.moveTo(1,cc.p(145,170));
				this.bottleline.runAction(move4);
			}else if(action ==ACTION_DO2||this.status == 2){
				this.showtip = new ShowTip("玻璃瓶受到的浮力大小小于自身和" +
						"\n瓶内水的重力大小，玻璃瓶下沉",cc.p(400,450));
				var move = cc.moveTo(1,cc.p(0,160));
				this.handle2.runAction(move);
				this.handle1.runAction(move.clone());

				var move2 = cc.moveTo(1,cc.p(0,200));
				var seq = cc.sequence(move2,cc.callFunc(function(){
					this.status = 3;
					this.flowNext();
					p.setEnable(true);
				},this));
				p.runAction(seq);	
				
				var move4 = cc.moveTo(1,cc.p(145,220));
				this.bottleline.runAction(move4);
				
			}else if(action ==ACTION_DO3||this.status == 3){
				this.showtip = new ShowTip("玻璃瓶受到的浮力大小大于自身和" +
						"\n瓶内水的重力大小，玻璃瓶上浮",cc.p(400,450));
				var move = cc.moveTo(2,cc.p(0,310));
				this.handle2.runAction(move);
				this.handle1.runAction(move.clone());

				var move1 = cc.moveTo(1,cc.p(200,140));
				var sq = cc.sequence(cc.delayTime(1),move1);
				this.flumeline.runAction(sq);

				var move2 = cc.moveTo(2,cc.p(0,450));
				var seq = cc.sequence(move2,cc.callFunc(function(){
					this.status = 1;
					this.flowNext(); 
					p.setEnable(true);
				},this));
				p.runAction(seq);		

				var move4 = cc.moveTo(2,cc.p(145,40));
				this.bottleline.runAction(move4);
			}
		break;
		

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});