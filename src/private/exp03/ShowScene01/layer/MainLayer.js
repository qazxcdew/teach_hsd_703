exp03.ShowMainLayer01 = cc.Layer.extend({
curIndex: 0,
ctor:function () {
	this._super();
	this.init();
	return true;
},
init:function(){
	var sv = new pub.ScrollView(this);
	sv.setOffestPos(cc.p(152,45 + 15));

	this.addlayout1(sv);
	this.addlayout2(sv);				
	this.addlayout3(sv);	
},	

addlayout1:function(parent){
	var layout1 = new ccui.Layout();//第一页
	layout1.setContentSize(cc.size(980, 600));//设置layout的大小
	parent.addLayer(layout1);
	var text = $.format("1.观察物体浮沉与物体自重的关系" +
			"\n（1）把一个空塑料瓶放在水盆里，观察它浮在水面上的位置，并在瓶外壁上做一记号。" +
			"\n（2）在另一空塑料瓶中放若干小石子，再放在水盆里，观察它浮沉情况，并与空塑料瓶在水盆里的浮沉情况作比较。" +
			"\n（3）在有小石子的塑料瓶内继续放石子，直到瓶下沉至水底。" +
			"\n（4）把两端的手指放开，水就会从高处的容器中流到低处的容器中。", 980, 30);

	var label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
	label1.setColor(cc.color(0, 0, 0, 250));
	label1.setPosition(layout1.width/2,layout1.height - label1.height/2);
	layout1.addChild(label1);	

	

	this.addbutton(layout1, TAG_BUTTON, "播放", cc.p(490,300));
	
},
addlayout2:function(parent){
	var layout2 = new ccui.Layout();//第一页
	layout2.setContentSize(cc.size(980, 600));//设置layout的大小
	parent.addLayer(layout2);
	var text = $.format("2.观察物体浮沉与物体排水多少的关系" +
			"\n（1）把铝皮牙膏壳剪开，摊平后弯折成船型。" +
			"\n（2）把船型铝皮放在盛有水的水盆中，观察现象。" +
			"\n（3）把船型铝皮取出，将他困成块状后放在盛有水的盆中，观察现象。", 980, 30);

	var label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
	label1.setColor(cc.color(0, 0, 0, 250));
	label1.setPosition(layout2.width/2,layout2.height - label1.height/2);
	layout2.addChild(label1);	

	this.addbutton(layout2, TAG_BUTTON1, "播放", cc.p(490,300));

},
addlayout3:function(parent){
	var layout3 = new ccui.Layout();//第三页
	layout3.setContentSize(cc.size(980, 600));
	parent.addLayer(layout3);

	var text3 = $.format("　　大量实验告诉我们，物体浸没在液体中时，究竟是浮是沉，是由该物体在液体中受到浮力和重力" +
			"之间的大小关系决定的。右图箭头长短代表力的大小。", 580,30);
	var label3 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
	label3.setAnchorPoint(0,0.5);
	label3.setColor(cc.color(0, 0, 0, 250));
	label3.setPosition(0,layout3.height - label3.height/2);		
	layout3.addChild(label3);	
	
	var sprite = new cc.Sprite("#fuchen.png");
	sprite.setPosition(780,440);
	layout3.addChild(sprite);

	
	var label4 = new cc.LabelTTF("如果浮沉条件用实心物体密度和液体密度之间的关系表示：" +
			"\n（1）浸没在液体中的物体的密度大于液体的密度，物体下沉。" +
			"\n（2）浸没在液体中的物体的密度等于液体的密度，物体悬浮。" +
			"\n（3）浸没在液体中的物体的密度小于液体的密度，物体上浮。",gg.fontName,gg.fontSize4);
	label4.setPosition(420,160);
	label4.setColor(cc.color(0, 0, 0, 250));
    layout3.addChild(label4);	

},
addbutton:function(parent,tag,str,pos){
	var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
	button.setPosition(pos);
	button.setTag(tag);
	var label = new cc.LabelTTF(str,gg.fontName,30);
	label.setColor(cc.color(0, 0, 0, 250));
	label.setPosition(button.width/2,button.height/2);		
	button.addChild(label);
},

callback:function(p){
	switch(p.getTag()){	
	case TAG_BUTTON:
	break;
	case TAG_BUTTON1:
		break;
	}
}

});
