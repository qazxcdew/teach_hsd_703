exp03.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		ch.gotoShow(TAG_EXP_03, TAG_MENU1,g_resources_show03, null, new exp03.ShowScene01(tag));
		break;
	case TAG_MENU2:		
		ch.gotoShow(TAG_EXP_03,  TAG_MENU2, g_resources_show03, null, new exp03.ShowScene02(tag));
		break;
	case TAG_MENU3:
		gg.teach_type = TAG_REAL;
		ch.gotoRun(TAG_EXP_03, TAG_MENU3, g_resources_run03, null, new exp03.RunScene01());
		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_03, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp03.teach_flow01 = 
	[
	 {
		 tip:"推到注射器，向玻璃瓶内注入水，玻璃瓶悬浮",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_HANDLE,
		      ],
		      action:ACTION_DO1
	 },
	 {
		 tip:"推到注射器，往玻璃瓶内继续注入水，玻璃瓶沉到水底",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_HANDLE,
		      ],
		      action:ACTION_DO2
	 },
	 {
		 tip:"拔出注射器，把水从玻璃瓶中抽出，玻璃瓶上浮",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_HANDLE,
		      ],action:ACTION_DO3
	 },
	 {
		 tip:"恭喜过关",
		 over:true
	 }
	 ]



