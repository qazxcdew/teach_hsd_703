exp03.ShowMainLayer02 = cc.Layer.extend({
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);				
		//this.addlayout3(sv);


	},
	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(980, 600));//设置layout的大小
		parent.addLayer(layout1);
		var text = $.format("1.潜艇" +
				"\n　　潜艇可以作为悬浮体停留在水下，也能作为漂浮体浮在水面。潜艇通过改变自身的重力达到浮沉自如的目的。" +
				"潜艇的内舱与外壳质检有一个水舱，位于潜艇两侧。水舱上装有阀门，打开阀门使海水进入水舱，潜艇的自重增大" +
				"，潜艇就下沉；反之用压缩空气把水舱内的水排出，潜艇的自重减小，潜艇就会上浮。", 980, 30);

		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setPosition(layout1.width/2,layout1.height - label1.height/2);
		layout1.addChild(label1);	


		this.addimage(layout1,"#qianting.png",cc.p(230,200));
		this.addimage(layout1,"#qianting1.png",cc.p(750,200));

	},
	addlayout2:function(parent){
		var layout2 = new ccui.Layout();//第一页
		layout2.setContentSize(cc.size(980, 600));//设置layout的大小
		parent.addLayer(layout2);
		var text = $.format("2.热气球" +
				"\n　　热气球一般有气囊和吊舱两部分。在地面时，加热器对气囊中的空气加热，气囊体积增大，" +
				"受到的浮力增大，当浮力大于自身重力时，热气球上升。热气球升空后，气囊空气逐渐冷却，体积减小，" +
				"当受到的浮力等于重力时，热气球悬浮；当温度下降到一定程度，热气球受到的浮力小于自身的重力，" +
				"热气球下降。", 980, 30);

		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setPosition(layout2.width/2,layout2.height - label1.height/2);
		layout2.addChild(label1);	
		
		this.addimage(layout2,"#reqiqiu.png",cc.p(300,200));
		this.addimage(layout2,"#reqiqiu1.png",cc.p(650,200));

		
	},
	addlayout3:function(parent){
		var layout3 = new ccui.Layout();//第三页
	    layout3.setContentSize(cc.size(980, 600));
	    parent.addLayer(layout3);

		

	},
	
	addimage:function(parent,str,pos){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
	},
	addlabel:function(parent,str,pos){
		var label = new  cc.LabelTTF(str,gg.fontName,gg.fontSize4);
		label.setPositon(pos);
		parent.addChild(label,5);		
	},
	addbutton:function(parent,tag,str,pos,labelx){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		button.addChild(label);
	},
	callback:function(p){
		switch(p.getTag()){	
		
		
		}
	}

});
