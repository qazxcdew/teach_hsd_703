exp01.Inoculator = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 7, TAG_INOCULATOR_NODE);
		this.init();
	},

	init : function(){
		var inoculator1 = new cc.Sprite("#inoculator1.png");
		inoculator1.setPosition(cc.p(0,0));
		this.addChild(inoculator1,3);
		inoculator1.setRotation(180);

		
		var inoculator2 =  new Button(this, 2, TAG_INOCULATOR, "#inoculator2.png",this.callback);
		inoculator2.setPosition(30,0);
		inoculator2.setRotation(180);

		
		var inoculator3 = new cc.Sprite("#inoculator3.png");
		inoculator3.setPosition(cc.p(0,0));
		this.addChild(inoculator3,1);
		inoculator3.setRotation(180);


	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_INOCULATOR:
			var zhengliunode = ll.run.getChildByTag(TAG_ZHENGLIU_NODE);
			if(action==ACTION_DO1){
				var seq = cc.sequence(cc.moveBy(1.5,cc.p(-50,0)),cc.callFunc(function(){
					var show = new ShowTip("气压升高" +
							"\n水不沸腾",cc.p(800,280));
					zhengliunode.stopAll();
					this.flowNext();				
				},this));
				p.runAction(seq);
			}else if(action==ACTION_DO2){
				var seq = cc.sequence(cc.moveBy(1.5,cc.p(50,0)),cc.delayTime(1),cc.callFunc(function(){
					zhengliunode.sesume();
					var show = new ShowTip("气压降低" +
							"\n水又沸腾",cc.p(800,280));			
				},this),cc.delayTime(3),cc.callFunc(function(){
					this.flowNext();	
				},this));
				p.runAction(seq);
			}
			break;
		}
	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});