exp01.Zhengliu = cc.Node.extend({
	posx:160,
	posy:160,
	ctor:function(p){
		this._super();
		p.addChild(this, 8, TAG_ZHENGLIU_NODE);
		this.init();
	},

	init : function(){		
		var iron =  new Button(this, 1, TAG_IRON, "#iron.png",this.callback);
		iron.setPosition(-68,145);
		
		var jiazi =  new Button(this, 6, TAG_JIAZI, "#jiazi.png",this.callback);
		jiazi.setPosition(-95,215);
		
		var lid = new cc.Sprite("#sai.png");//橡胶塞
		lid.setPosition(-50,285);
		this.addChild(lid,1);
		
		var daoguan = new cc.Sprite("#daoguan.png");//导管
		daoguan.setPosition(-10,300);
		this.addChild(daoguan);
		
		var xiangjiaoguan = new cc.Sprite("#xiangjiao.png");//橡胶管
		xiangjiaoguan.setPosition(60,321);
		this.addChild(xiangjiaoguan,6);
			
		var zhengliu =  new Button(this, 2, TAG_ZHENGLIU, "#zhengliu.png",this.callback);//蒸馏烧瓶
		zhengliu.setPosition(-50,200);
				
		var cly_line1 = new cc.Sprite("#cly_line1.png");//水位线
		cly_line1.setPosition(-50,160);
		cly_line1.setScale(1.6,0.5);
		this.addChild(cly_line1);		
	},
	addqipao:function(pos,time){	//蒸馏烧瓶冒泡	
		var qipao = new cc.Sprite("#bubble1.png");
		qipao.setPosition(pos);
		this.addChild(qipao,2);
		var seq = cc.sequence(cc.spawn(cc.moveBy(time,cc.p(0,45)),cc.scaleTo(time,1.2)),cc.fadeOut(0.2),cc.callFunc(function(){
			qipao.removeFromParent(true);
		},this));
		qipao.runAction(seq);
	},
	addposX:function(){
		var posX = Math.random()*40;
		return (posX-70);		
	},
	
	stopAll:function(){
		this.pause();//暂停所有的调度过的选择器，动作和事件监听器。 这个方法被onExit方法在内部调用。 
	},
	
	sesume:function(){
		this.resume();//恢复所有的调度过的选择器，动作和事件监听器。 这个方法被onEnter方法在内部调用。
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
//		case TAG_ZHENGLIU:
//			break;

//		case TAG_DOWN:
//			var show = new ShowTip(ll.run,"进水",25,cc.p(670,210),TAG_SHOW1);
//			var show = new ShowTip(ll.run,"出水",25,cc.p(550,390),TAG_SHOW1);
//			var water = new cc.Sprite("#cly_line1.png");
//			water.setScale(0.3,0.5);
//			water.setPosition(cc.p(147,155));
//			this.addChild(water,5);
//			var seq = cc.sequence(cc.spawn(cc.moveTo(1,cc.p(140,170)),cc.scaleTo(1,1.2,0.5)),cc.moveTo(2,cc.p(58,205)),
//					cc.spawn(cc.moveTo(1,cc.p(47,215)),cc.scaleTo(1,0.6,0.5)),cc.spawn(cc.moveTo(0.3,cc.p(55,217)),cc.scaleTo(0.3,0.3,0.5)),
//					cc.fadeOut(0.3),cc.callFunc(function(){
//						//water.removeFromParent(true);
//						this.flowNext();
//					},this));
//			water.runAction(seq);
//			
//			
//			var delay = cc.delayTime(0.35);
//			var seq1 = cc.sequence(delay,cc.callFunc(function(){
//				this.addshuiwei(155,160,0.08);
//			},this),delay,cc.callFunc(function(){
//				this.addshuiwei(145,166,0.12);
//			},this),delay,cc.callFunc(function(){
//				this.addshuiwei(130,172,0.15);
//			},this),delay,cc.callFunc(function(){
//				this.addshuiwei(115,178,0.15);
//			},this),delay,cc.callFunc(function(){
//				this.addshuiwei(100,184,0.15);
//			},this),delay,cc.callFunc(function(){
//				this.addshuiwei(85,190,0.15);
//			},this),delay,cc.callFunc(function(){
//				this.addshuiwei(70,196,0.15);
//			},this),delay,cc.callFunc(function(){
//				this.addshuiwei(55,204,0.12);
//			},this),delay,cc.callFunc(function(){
//				this.addshuiwei(50,210,0.08);
//			},this),delay);
//			
//			water.runAction(seq1);	
//		break;
		}
	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});