exp01.Lamp = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_LAMP_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.lamplid= new Button(this, 11, TAG_LAMP_LID, "#lampLid.png",this.callback);
		this.lamplid.setPosition(0,30);

		this.lamp= new Button(this, 10, TAG_LAMP, "#lampBottle.png",this.callback);
		this.lamp.setPosition(0,0);

		this.match= new Button(this, 1, TAG_MATCH, "#match.png",this.callback);
		this.match.setScale(0.2);
		this.match.setPosition(100,-20);



	},
	fire:function(){
		var frames=[];
		for (i=1;i<=3;i++){
			var str ="fire2/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建

		var fire = new cc.Sprite("#fire2/1.png");
		fire.setPosition(50,86);
		fire.setAnchorPoint(0.5,0);
	
		ll.run.lamp.lamp.addChild(fire,1,TAG_SHOW);
		fire.runAction(action.repeatForever());
	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){		
		case TAG_MATCH:
			var match = new cc.Sprite("#match1.png");
			match.setPosition(155,26);
			match.setScale(0.2);
			match.setAnchorPoint(1,1);
			this.addChild(match,2);
			var seq = cc.sequence(cc.moveTo(0.3,cc.p(180,13)),cc.callFunc(function(){
				match.setSpriteFrame("match2.png");
			},this),cc.moveTo(0.8,cc.p(52,65)),cc.callFunc(function(){
				this.fire();
				match.removeFromParent(true);
				gg.flow.next();
			},this));

			match.runAction(seq);
			break;
		case TAG_LAMP_LID:
			if(action == ACTION_DO1){
				var move = cc.moveTo(0.5,cc.p(0,60));
				var move1 = cc.moveTo(0.5,cc.p(-40,50));
				var move2 = cc.moveTo(0.5,cc.p(-80,-30));
				var seq = cc.sequence(move,move1,move2,cc.callFunc(this.flowNext ,this));				
			}else if (action == ACTION_DO2){				
				var move = cc.moveTo(0.5,cc.p(-40,50));
				var move1 = cc.moveTo(0.5,cc.p(0,80));
				var move2 = cc.moveTo(0.5,cc.p(0,30));
				var seq = cc.sequence(move,move1,cc.callFunc(function(){
					this.lamp.getChildByTag(TAG_SHOW).runAction(cc.scaleTo(0.5,0));
				},this),move2,move1,move2,cc.callFunc(this.flowNext ,this));	
			}
			
			p.runAction(seq);

			break;
		case TAG_LAMP:			
			if(action==ACTION_DO1){
				var clock = ll.run.clock;
				clock.setSpriteFrame("clock1.png");
				var zhengliunode = ll.run.getChildByTag(TAG_ZHENGLIU_NODE);
				var seq = cc.sequence(cc.moveTo(1,cc.p(250,5)),cc.callFunc(function(){
					zhengliunode.schedule(function(){
						zhengliunode.addqipao(cc.p(zhengliunode.addposX(),115),0.5);
					}, 0.05, 80, 3);	
					clock.doing();
				},this),cc.delayTime(7),cc.callFunc(function(){
					zhengliunode.schedule(function(){
						clock.stop();
						zhengliunode.addqipao(cc.p(zhengliunode.addposX(),115),0.3);
					}, 0.02, cc.REPEAT_FOREVER);	
					this.flowNext();
				},this));

				p.runAction(seq);
			}
			else if(action==ACTION_DO2){
				var zhengliunode = ll.run.getChildByTag(TAG_ZHENGLIU_NODE);
				var seq = cc.sequence(cc.moveTo(1,cc.p(0,0)),cc.callFunc(function(){
					this.flowNext();
				},this),cc.delayTime(2),cc.callFunc(function(){
					//	zhengliunode.stopAll();
				},this));

				p.runAction(seq);
			}


			break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});