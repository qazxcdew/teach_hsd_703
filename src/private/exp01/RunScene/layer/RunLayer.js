exp01.RunLayer1 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		this.lib.loadBg([{
			tag:TAG_LIB_IRON2,
			checkright:true,	//实验所需器材添加checkright标签		
		},
		{
			tag:TAG_LIB_CONICAL,

		},
		{
			tag:TAG_LIB_FLASK,
			checkright:true,
		},
		{
			tag:TAG_LIB_INOCULATOR,
			checkright:true,
		},
		{
			tag:TAG_LIB_YIBEI,

		},
		{
			tag:TAG_LIB_BEAKER,
		},
		{
			tag:TAG_LIB_ROD,
		},
		{
			tag:TAG_LIB_AMPULLA,
		},
		{
			tag:TAG_LIB_LAMP,
			checkright:true,
		},
		{
			tag:TAG_LIB_ZHENGFA,
		}
		]);

		this.lamp = new exp01.Lamp(this);
		this.lamp.setVisible(false);
		this.lamp.setPosition(400,150);

		this.zhengliu = new exp01.Zhengliu(this);
		this.zhengliu.setVisible(false);
		this.zhengliu.setPosition(700,120);

		this.inoculator = new exp01.Inoculator(this);
		this.inoculator.setVisible(false);
		this.inoculator.setPosition(855,430);
	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.zhengliu;
		var node2 = ll.run.lamp;
		var node3 = ll.run.inoculator;


		checkVisible.push(node1,node2,node3);

		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});