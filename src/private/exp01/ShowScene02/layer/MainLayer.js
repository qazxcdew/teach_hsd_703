exp01.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);				
		this.addlayout3(sv);
	},	
	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);

		label1 = new cc.LabelTTF("1.把装满水的容器放在较高处，另一个没有盛水的容器放在较低处；" +
				"\n2.将整个管子浸在生谁的容器中，直到管中所有空气都排出；" +
				"\n3.用手指将水管的两端堵住，并将一段移到另一个空容器中；" +
				"\n4.把两端的手指放开，水就会从高处的容器中流到低处的容器中。",gg.fontName,gg.fontSize4);
		label1.setPosition(layout1.width/2,layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		layout1.addChild(label1);	

	    var bowl = new cc.Sprite("#bowl1.png");
	    bowl.setPosition(750,300);
	    bowl.setScale(0.8);
	    layout1.addChild(bowl,5);
	    
	    this.bowlwater = new cc.Sprite("#water.png");
	    this.bowlwater.setPosition(175,30);
	    this.bowlwater.setAnchorPoint(0.5,0);
	    this.bowlwater.setScale(1.1,1.5);
	    bowl.addChild( this.bowlwater,5);
	    
	    var bowl1 = new cc.Sprite("#bowl2.png");
	    bowl1.setPosition(750,300);
	    bowl1.setScale(0.8);
	    layout1.addChild(bowl1,10);
	        
	    var bowl2 = new cc.Sprite("#bowl1.png");
	    bowl2.setPosition(280,80);
	    bowl2.setScale(0.8);
	    layout1.addChild(bowl2,5);
	    
	    this.bowlwater1= new cc.Sprite("#water.png");
	    this.bowlwater1.setPosition(175,33);
	    this.bowlwater1.setAnchorPoint(0.5,0);
	    this.bowlwater1.setScale(0.8,0.6);
	    this.bowlwater1.setVisible(false);
	    bowl2.addChild(this.bowlwater1,5);
	    
	    var bowl3 = new cc.Sprite("#bowl2.png");
	    bowl3.setPosition(280,80);
	    bowl3.setScale(0.8);
	    layout1.addChild(bowl3,10);
	    
	    var duct = new cc.Sprite("#duct.png");
	    duct.setPosition(510,210);
	    duct.setScale(0.8,0.7);
	    layout1.addChild(duct,7);
	    
	    this.addbutton(layout1,TAG_BUTTON, "播放", cc.p(650,30));

	},
	addlayout2:function(parent){
		var layout2 = new ccui.Layout();//第二页
		layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(layout2);

		this.hongxi1 = new cc.Sprite("#hongxi.png");
		this.hongxi1.setScale(0.8,0.7);
		this.hongxi1.setPosition(490,layout2.height - this.hongxi1.height/2);
		layout2.addChild(this.hongxi1,5);	


		var text2 = $.format("　　虹吸现象在生产和生活中运用很广泛，农业生产中利用它能将渠道中水引入田中灌溉。" +
				"可以通过虹吸管换掉鱼缸中的水，有的自动抽水马桶就是利用这个原理工作的。", 980,30);
		label2 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);
		label2.setColor(cc.color(0, 0, 0, 250));
		$.down(this.hongxi1,label2,20);
		layout2.addChild(label2);	
	},
	addlayout3:function(parent){
		layout3 = new ccui.Layout();//第三页
		layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(layout3);

		var text3 = $.format("　　初次进入海拔3000米以上的高山地区的人，会出现高山反应，也叫高山病。" +
				"高山病的症状因人而异。主要包括：头晕、头痛、耳鸣、恶心、呕吐、脉搏和呼吸加快、" +
				"四肢麻木等。严重的会陷入昏迷，甚至死亡", 980,30);
		label3 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		label3.setColor(cc.color(0, 0, 0, 250));
		label3.setPosition(layout3.width/2,layout3.height-label3.height/2);		
		layout3.addChild(label3);	

		this.gaoshan = new cc.Sprite("#gaoshan.png");
		this.gaoshan.setPosition(280,350);
	    layout3.addChild(this.gaoshan,5);	

		this.gaoshan1 = new cc.Sprite("#gaoshan1.png");
		this.gaoshan1.setPosition(680,350);
		layout3.addChild(this.gaoshan1,5);	

		this.addbutton(layout3, TAG_BUTTON1, "原因", cc.p(40,200));


		var text4 = $.format("　　：因为高山地区空气稀薄，大气压强低，人们呼吸时吸入的氧气的分压也低，造成肺泡中的氧" +
				"分压降低，血液中含量减少。从而使人的神经系统、呼吸系统和心血管系统出现障碍。", 980,30);
		this.label4 = new cc.LabelTTF(text4,gg.fontName,gg.fontSize4);
		this.label4.setColor(cc.color(0, 0, 0, 250));
		this.label4.setPosition(490,160);
		this.label4.setVisible(false);
		layout3.addChild(this.label4);	
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:
			p.setEnable(false);
			this.bowlwater.setScale(1.1,1.5);
			this.bowlwater.setPosition(cc.p(175,30));

			this.bowlwater1.setScale(0.8,0.6);			
			this.bowlwater1.setVisible(true);
			this.bowlwater.runAction(cc.spawn(cc.scaleTo(2,1,1),cc.moveTo(2,cc.p(175,33))));
			this.bowlwater1.runAction(cc.scaleTo(2,1));
			var seq = cc.sequence(cc.delayTime(2),cc.callFunc(function(){
											
				p.setEnable(true);
			},this));
			p.runAction(seq);
		break;
		case TAG_BUTTON1:
			p.setOpacity(0);
			p.setEnable(false);
			this.label4.setVisible(true);
		break;	
		}
	}

	
});
