exp01.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		ch.gotoShow(TAG_EXP_01,TAG_MENU1 ,g_resources_show01, null, new exp01.ShowScene01(tag));
		break;
	case TAG_MENU2:
		gg.teach_type = TAG_REAL;
		ch.gotoRun(TAG_EXP_01, TAG_MENU2,g_resources_run01, null, new exp01.RunScene01());
		break;
	case TAG_MENU3:
		ch.gotoShow(TAG_EXP_01, TAG_MENU3, g_resources_show01, null, new exp01.ShowScene02(tag));
		break;
		gotoShow
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_01, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp01.teach_flow01 = 
	[
 {
	 tip:"选择铁架台",
	 tag:[
	      TAG_LIB,
	      TAG_LIB_BG1,	   
	      ],
	      canClick:true
 },
 {
	 tip:"选择烧瓶",
	 tag:[
	      TAG_LIB,
	      TAG_LIB_BG3,
	      ],
	      canClick:true
 },
 {
	 tip:"选择注射器",
	 tag:[
	      TAG_LIB,
	      TAG_LIB_BG4,
	      ],
	      canClick:true
 },	
 {
	 tip:"选择酒精灯",
	 tag:[
	      TAG_LIB,
	      TAG_LIB_BG9,
	      ],
	      canClick:true
 },		 
 {
	 tip:"点击确定，开始实验",
	 tag:[
	      TAG_LIB,
	      TAG_SURE
	      ]
 },
 {
	 tip:"取下酒精灯帽",
	 tag:[TAG_LAMP_NODE,
	      TAG_LAMP_LID],action:ACTION_DO1
 },
 {
	 tip:"点燃酒精灯",
	 tag:[TAG_LAMP_NODE,
	      TAG_MATCH]
 },
 {
	 tip:"用酒精灯加热蒸馏烧瓶",
	 tag:[
	      TAG_LAMP_NODE,
	      TAG_LAMP
	      ],action:ACTION_DO1
 },
 {
	 tip:"用注射器向瓶内充气，观察现象",
	 tag:[
	      TAG_INOCULATOR_NODE,
	      TAG_INOCULATOR
	      ],action:ACTION_DO1
 }, 
 {
	 tip:"取下酒精灯",
	 tag:[
	      TAG_LAMP_NODE,
	      TAG_LAMP],action:ACTION_DO2
 },
 {
	 tip:"熄灭酒精灯",
	 tag:[
	      TAG_LAMP_NODE,
	      TAG_LAMP_LID],action:ACTION_DO2
 },	 
 {
	 tip:"用注射器从瓶内吸气，观察现象",
	 tag:[
	      TAG_INOCULATOR_NODE,
	      TAG_INOCULATOR],action:ACTION_DO2
 }, 
 {
	 tip:"恭喜过关",
	 over:true
 }
]




