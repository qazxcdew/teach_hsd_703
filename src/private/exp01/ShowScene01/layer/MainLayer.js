exp01.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
	},	
	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);

		var text1 = $.format("　　托里拆利实验，在一根长约1米，一端封闭的玻璃管内注满水银，" +
				"用带有指套手指堵住管口，然后倒置插在水银槽中，移开手指，管内水银液面会下降，当" +
				"管内外水银面高度差约为760mm，水银面就不再下降了。", 980,30);
		label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setPosition(gg.c_width,gg.height-label1.height/2 - 130);
		this.addChild(label1);	

		this.daqiya = new cc.Sprite("#daqiyaceding.png");
		this.addChild(this.daqiya,5);	
		$.down(label1,this.daqiya,20);

		var text2 = $.format("　　由于管内水银柱产生的压强与大气压强相平衡，使水银面不再变化。" +
				"如果当大气压强减小时，原先的平衡被破坏，管内水银柱下降到使它产生的压强与减小了的大气压强相等，" +
				"达到新的平衡，同理当大气压强增大时，管内水银柱将升高，以达到新平衡。所以可以利用托里拆利实验管中水银柱的" +
				"高度，来随时测量当时的大气压强。", 980,30);
		label2 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);
		label2.setColor(cc.color(0, 0, 0, 250));
		this.addChild(label2);	
		$.down(this.daqiya,label2,20);					
	}

});
