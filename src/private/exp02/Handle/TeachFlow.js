exp02.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		ch.gotoShow(TAG_EXP_02, TAG_MENU1, g_resources_show02, null, new exp02.ShowScene01(tag));
		break;
	case TAG_MENU2:
		gg.teach_type = TAG_REAL;
		ch.gotoRun(TAG_EXP_02, TAG_MENU2, g_resources_run02, null, new exp02.RunScene01());
		break;
	case TAG_MENU3:
		ch.gotoShow(TAG_EXP_02, TAG_MENU3, g_resources_show02, null, new exp02.ShowScene02(tag));
		break;
	case TAG_MENU4:
		ch.gotoShow(TAG_EXP_02, TAG_MENU4, g_resources_show02, null, new exp02.ShowScene03(tag));
		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_02, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp02.teach_flow01 = 
	[
	 {
		 tip:"推动小车,观察小车滑动的距离",
		 tag:TAG_TIP
	 },
	 {
		 tip:"恭喜过关",
		 over:true
	 }
]




