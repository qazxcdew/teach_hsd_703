exp02.RunMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		ll.run = null;
		ll.main = this;
		this.loadTip();		
		this.loadRun();
		this.loadFlow();
		return true;
	},
	
	loadTip:function(){
		ll.tip = new TipLayer(this);
	},
	loadRun:function(){
		ll.run = new exp02.RunLayer1(this);
	},
	loadFlow:function(){
		gg.flow.setMain(this);
		gg.flow.start();
	},
	over: function (){
		ll.tip.over();       	
		ll.tip.back_frame.open();	    	
		// 提交成绩
		net.saveScore();
	},
});
