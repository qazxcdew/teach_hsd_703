exp02.RunLayer1 = exp02.BaseLayer01.extend({
	arr:null,
	scene:null,
	ctor:function (parent) {
		this._super(parent);
		this.init();
	},
	init:/**
	 * 初始化,
	 */
		function () {
		this._super();
		cc.log("RunLayer1初始化");
		this.initPhysics();
		this.scheduleUpdate();

		this.loadMenu();

		this.board = new cc.Sprite("#wood.png");
		this.board.setPosition(gg.c_width, 580);
		this.addChild(this.board);

		var boardLabel = new cc.LabelTTF("木板", gg.fontName, gg.fontSize);
		boardLabel.setPosition(this.board.width * 0.7, -20);
		this.board.addChild(boardLabel);

		// 棉布
		this.board2 = new cc.Sprite("#wood.png");
		this.board2.setPosition(gg.c_width, 380);
		this.addChild(this.board2);
		var cotton = new cc.Sprite("#cotton.png");
		cotton.setPosition(569, 43);
		this.board2.addChild(cotton);
		var cottonLabel = new cc.LabelTTF("棉布", gg.fontName, gg.fontSize);
		cottonLabel.setPosition(this.board2.width * 0.7, -20);
		this.board2.addChild(cottonLabel);

		// 毛巾
		this.board3 = new cc.Sprite("#wood.png");
		this.board3.setPosition(gg.c_width, 180);
		this.addChild(this.board3);
		var towel = new cc.Sprite("#towel.png");
		towel.setPosition(569, 43);
		this.board3.addChild(towel);
		var towelLabel = new cc.LabelTTF("毛巾", gg.fontName, gg.fontSize);
		towelLabel.setPosition(this.board3.width * 0.7, -20);
		this.board3.addChild(towelLabel);

		this.car = new exp02.Car(this, "#car.png");
		this.car.setPos(cc.p(255, 576 +32+ this.car.height * 0.5));
		this.car.shape.setCollisionType(COLLISION_TYPE_CAR);
		this.car_hand = new cc.Sprite("#hand.png");
		this.car_hand.setPosition(cc.p(-35,45));
		this.car.addChild(this.car_hand);
		this.car_hand.setRotation(-30);

		this.car2 = new exp02.Car(this, "#car.png");
		this.car2.setPos(cc.p(255, 376 + 34+this.car2.height * 0.5));
		this.car2.shape.setCollisionType(COLLISION_TYPE_CAR2);
		this.car2_hand = new cc.Sprite("#hand.png");
		this.car2_hand.setPosition(cc.p(-35,45));
		this.car2.addChild(this.car2_hand);
		this.car2_hand.setRotation(-30);

		this.car3 = new exp02.Car(this, "#car.png");
		this.car3.setPos(cc.p(255, 176 + 34+this.car3.height * 0.5));
		this.car3.shape.setCollisionType(COLLISION_TYPE_CAR3);
		this.car3_hand = new cc.Sprite("#hand.png");
		this.car3_hand.setPosition(cc.p(-35,45));
		this.car3.addChild(this.car3_hand);
		this.car3_hand.setRotation(-30);
	},
	loadMenu:function(){
		var go = new Button2(this,10,TAG_RUN_GO,"#same_time.png",this.callback);
		go.setPosition(gg.width - 40 - go.width * 0.5, 40 + go.height * 0.5);

		var restart = new Button2(this,10,TAG_RUN_RESTART,"#replay.png",this.callback);
		restart.right(go, 30);
		restart.setPosition(gg.width - 40 - go.width * 0.5, 100 + restart.height * 0.5);

		if(gg.teach_type == TAG_LEAD){
			this.schedule(this.updateSecond, 1);
		}

		var go1 = new Button2(this,10,TAG_RUN_GO1,"#play.png",this.callback);
		go1.setPosition(120, 575);

		var go2 = new Button2(this,10,TAG_RUN_GO2,"#play.png",this.callback);
		go2.setPosition(120, 375);

		var go3 = new Button2(this,10,TAG_RUN_GO3,"#play.png",this.callback);
		go3.setPosition(120, 175);
	},
	initPhysics:/**
	 * 初始化物理引擎
	 */
		function() {
		gg.space = new cp.Space();
		gg.space.gravity = cp.v(0, -400);// 设置重力加速度

		// 墙壁
		this.wallGround = new cp.SegmentShape(gg.space.staticBody,
				cp.v(0, 520),// start point
				cp.v(12800, 520),// MAX INT:4294967295
				0);// thickness of wall
		gg.space.addStaticShape(this.wallGround);
//		地面摩擦力系数
		this.wallGround.setFriction(0.2);
//		斜坡
		var wallSlope = new cp.SegmentShape(gg.space.staticBody,
				cp.v(0, 719),// start point
				cp.v(445, 520),// MAX INT:4294967295
				0);// thickness of wall
		gg.space.addStaticShape(wallSlope);

		// 强制停止的墙(有些浏览器BUG)
		var wallStop = new cp.SegmentShape(gg.space.staticBody,
				cp.v(1000, 574),// start point
				cp.v(1000, 520),// MAX INT:4294967295
				0);// thickness of wall
//		gg.space.addStaticShape(wallStop);
		// 碰撞检测, 类型
		wallStop.setCollisionType(COLLISION_TYPE_STOP_WALL);


		this.wallGround2 = new cp.SegmentShape(gg.space.staticBody,
				cp.v(0, 320),// start point
				cp.v(12800, 320),// MAX INT:4294967295
				0);// thickness of wall
		gg.space.addStaticShape(this.wallGround2);
//		地面摩擦力系数
		this.wallGround2.setFriction(0.3);

		var wallSlope2 = new cp.SegmentShape(gg.space.staticBody,
				cp.v(0, 524),// start point
				cp.v(445, 320),// MAX INT:4294967295
				0);// thickness of wall
		gg.space.addStaticShape(wallSlope2);

		var wallStop2 = new cp.SegmentShape(gg.space.staticBody,
				cp.v(720, 374),// start point
				cp.v(720, 320),// MAX INT:4294967295
				0);// thickness of wall
//		gg.space.addStaticShape(wallStop2);
		wallStop2.setCollisionType(COLLISION_TYPE_STOP_WALL);

		this.wallGround3 = new cp.SegmentShape(gg.space.staticBody,
				cp.v(0, 120),// start point
				cp.v(12800, 120),// MAX INT:4294967295
				0);// thickness of wall
		gg.space.addStaticShape(this.wallGround3);
//		地面摩擦力系数
		this.wallGround3.setFriction(0.4);

		var wallSlope3 = new cp.SegmentShape(gg.space.staticBody,
				cp.v(0, 324),// start point
				cp.v(445, 120),// MAX INT:4294967295
				0);// thickness of wall
		gg.space.addStaticShape(wallSlope3);

		var wallStop3 = new cp.SegmentShape(gg.space.staticBody,
				cp.v(600, 174),// start point
				cp.v(600, 120),// MAX INT:4294967295
				0);// thickness of wall
//		gg.space.addStaticShape(wallStop3);
		wallStop3.setCollisionType(COLLISION_TYPE_STOP_WALL);
	},
	update: function(dt) {
		var steps = 3;
		dt /= steps;
		for (var i = 0; i < 3; i++){
			gg.space.step(dt);
		}
		this.car.update(dt);
		this.car2.update(dt);
		this.car3.update(dt);
	},
	updateSecond:function(){
		// 检测是否下一步
		if(this.checkOver()){
			this.unschedule(this.updateSecond);
			this.mude.open();
			this.mude.buttonSetEnable(false);
			ll.tip.tipItem.setEnable(false);
		}
		var vel = this.car.getBody().getVel();
	},
	checkOver:/**
	 * 是否通关
	 * 
	 * @returns {Boolean}
	 */
		function(){
		if(!this.car.slide
				|| this.car.getBody().getVel().x > 1){
			return false;
		}
		if(!this.car2.slide
				|| this.car2.getBody().getVel().x > 1){
			return false;
		}
		if(!this.car3.slide
				|| this.car3.getBody().getVel().x > 1){
			return false;
		}
		return true;
	},
	callback:function(p){
		this.action(p);
		switch (p.getTag()) {
		case TAG_RUN_GO1:
			cc.log("1号小车");
			this.car_hand.setVisible(false);
			this.car.run();
			break;
		case TAG_RUN_GO2:
			cc.log("2号小车");
			this.car2.run();
			this.car2_hand.setVisible(false);
			break;
		case TAG_RUN_GO3:
			cc.log("3号小车");
			this.car3.run();
			this.car3_hand.setVisible(false);
			break;
		case TAG_RUN_GO:
			cc.log("同时移动");
			this.car.run();
			this.car_hand.setVisible(false);
			this.car2.run();
			this.car2_hand.setVisible(false);
			this.car3.run();
			this.car3_hand.setVisible(false);
			break;
		case TAG_RUN_RESTART:
			cc.log("复位");
			this.car.restart();
			this.car_hand.setVisible(true);
			this.car2.restart();
			this.car2_hand.setVisible(true);
			this.car3.restart();
			this.car3_hand.setVisible(true);
			break;
		default:
			break;
		}
	},
	action:function(p){
		var seq = cc.sequence(cc.scaleTo(0.1, 0.7),cc.scaleTo(0.1, 1));
		p.runAction(seq);
	},
	flash:function (bt){
		var fade1 = cc.fadeTo(0.3, 50);
		var fade2 = cc.fadeTo(0.3, 255);
		var seq = cc.sequence(fade1,fade2);
		var flash = cc.repeatForever(seq);
		bt.runAction(flash);
	},
	stop:function (bt){
		bt.stopAllActions();
		bt.setOpacity(1);
	},
	destroy:/**
	 * 销毁
	 */
		function(){
		this._super();
	},
	onExit:function(){
		this._super();
	}
});