/**
 * 基础类,处理公共方法,以及纹理等
 */
exp02.BaseLayer01 = cc.Layer.extend({
	nodeName:"未命名",
	ctor:function (parent) {
		this._super();
		parent.addChild(this, 10);
	},
	init:/**
	 * 初始化,
	 */
		function () {
		cc.log("BaseLayer初始化");
		ll.run = this;
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this);
		this.callNext.retain();
		this.callKill.retain();

		this.clock = new Clock(this);
	},
	destroy:/**
	 * 销毁
	 */
		function(){
		this.callNext.release();	
		this.callKill.release();
		this.showFinish();
		this.scheduleOnce(function(){
			this.removeFromParent(true);	
		}, 2);
	}
});