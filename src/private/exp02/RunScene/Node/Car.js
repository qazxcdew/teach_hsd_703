ACTION_TYPE_RUNNING = 5555;
exp02.Car = cc.PhysicsSprite.extend({
	d_scale:1,
	slide:/**
	 * 是否滑动
	 */
		false,
		reset:/**
		 * 是否在初始位置
		 */
			true,
			ctor:function(target,fileName){
				this._super("#car/car_body.png");
				this.setScale(this.d_scale);
				target.addChild(this);
				this.slide = false;
				this.reset = true;
				this.initBody();
				this.setRotation(24);
			},
			initBody:function(){
				var box = cp.momentForBox(1, this.width * this.d_scale, this.height * this.d_scale);
				this.body = new cp.Body(1, box);
				this.shape = new cp.BoxShape(this.body, this.width * this.scale, this.height * this.scale);
				// 物体摩擦力系数
				this.shape.setFriction(0.72);
				gg.space.addShape(this.shape);
				this.setBody(this.body);
				if(!!this.orig_p){
					this.body.p = cc.p(this.orig_p.x, this.orig_p.y);
				}
				var vel = this.getBody().getVel();
				this.orig_vel = cp.v(vel.x, vel.y);

				this.lunzi = new cc.Sprite("#car/1.png");
				this.lunzi.setPosition(this.width * 0.5, this.height * 0.5);
				this.addChild(this.lunzi);
				this.lunzi.retain();
			},
			run:function(){
				if(this.reset){
					this.slide = true;
					this.reset = false;
					gg.space.addBody(this.body);
					this.bodyRemove = true;
				}
			},
			runStop:function(){
				this.getBody().setVel(cp.v(this.orig_vel.x, this.orig_vel.y));
			},
			setPos:function(pos){
				this.getBody().p = pos;
				this.orig_p = cc.p(pos.x, pos.y);
			},
			restart:function(){
				if(this.bodyRemove){
					gg.space.removeBody(this.body);
					this.bodyRemove = false;
				}

				this.reset = true;
				this.slide = false;
				this.prevPosX = 0;
				this.getBody().p = cc.p(this.orig_p.x, this.orig_p.y);
				this.setRotation(24);
				this.runStop();
			},
			prevPosX:/**
			 * 上一帧的位置
			 */
				0,
				frame:0,
				update:function(dt){
					if(this.slide){
						this.frame++;
						var speed = this.getPositionX() - this.prevPosX; 
						if(this.prevPosX > 0 
								&& speed < 0.02 ){
							// 停止运动
							cc.log("小车已停止");
							this.slide = false;
							this.prevPosX = 0;
							this.runStop();
							return;
						}
						this.prevPosX = this.getPositionX();
						var maxSpeed = 4;
						speed = speed > maxSpeed ? maxSpeed : speed;
						this.updateSpeed(maxSpeed - speed + 1);
					}
					this.updateLunZi();
				},
				updateSpeed:function(speed){
					speed = parseInt(speed);
					if(this.frame % speed != 0){
						return;
					}
					var i = this.frame / speed % 20 + 1;
					var str = "car/" + i + ".png";
					this.lunzi.setSpriteFrame(str);
				},
				updateLunZi:/**
				 * 刷新轮子位置,JSB的BUG
				 */
					function(){
					this.removeChild(this.lunzi);
					this.addChild(this.lunzi);
				},
				onExit:function(){
					this._super();
					this.lunzi.release();
				}
});