exp02.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		
	},	

	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);

		var text1 = $.format("　　当要求物体运动速度的大小或方向发生变化时，都必须对物体施加力的作用。" +
				"物体速度大小的变化或方向的变化，称之为物体运动状态的改变。", 980,30);
		label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setPosition(500,layout1.height - label1.height/2 );
		layout1.addChild(label1);	

		var muban = new cc.Sprite("#muban.png"); //木板
		muban.setPosition(235,350);
		muban.setScale(0.65);
		layout1.addChild(muban);

		var qiu = new cc.Sprite("#qiu.png"); //A点球
		qiu.setPosition(180,230);
		muban.addChild(qiu);

		this.qiu = new cc.Sprite("#qiu.png"); //A点球
		this.qiu.setPosition(180,230);
		muban.addChild(this.qiu);

		this.label3 = new cc.LabelTTF("A",gg.fontName,gg.fontSize4);
		muban.addChild(this.label3);
		this.label3.setPosition(170,200);

		this.line1 = new cc.Sprite("#line1.png"); //直线
		this.line1.setPosition(390,225);
		this.line1.setVisible(false);
		muban.addChild(this.line1);

		this.qiu1 = new cc.Sprite("#qiu.png"); //B点球
		this.qiu1.setPosition(600,230);
		this.qiu1.setVisible(false);
		muban.addChild(this.qiu1);

		this.label4 = new cc.LabelTTF("B",gg.fontName,gg.fontSize4);
		this.label4.setVisible(false);
		muban.addChild(this.label4);
		this.label4.setPosition(580,210);

		this.line2 = new cc.Sprite("#line2.png"); //曲线
		this.line2.setPosition(360,185);
		this.line2.setVisible(false);
		muban.addChild(this.line2);

		this.qiu2 = new cc.Sprite("#qiu.png"); //C点球
		this.qiu2.setPosition(545,135);
		this.qiu2.setVisible(false);
		muban.addChild(this.qiu2);

		this.label5 = new cc.LabelTTF("C",gg.fontName,gg.fontSize4);
		this.label5.setVisible(false);
		muban.addChild(this.label5);
		this.label5.setPosition(525,110);

		this.citie = new cc.Sprite("#citie.png"); //citie
		this.citie.setPosition(360,100);
		this.citie.setVisible(false);
		muban.addChild(this.citie);

//		this.button = new ButtonScale(this,"#unsel.png",this.callback);
//		this.button.setPosition(350,200);
//		var label = new cc.LabelTTF("播放",gg.fontName,30);
//		label.setColor(cc.color(0, 0, 0, 250));
//		label.setPosition(this.button.width/2,this.button.height/2);
//		this.button.addChild(label);
		
		this.addbutton(layout1, TAG_BUTTON, "播放", cc.p(350,200));

		var text2 = $.format("　　光滑的水平桌面上运动的小球，在不受外力作用时，沿直线从A点运动到B点。" +
				"若在其原先运动轨迹的侧面放置一条形磁铁，小球会向磁铁方向偏转，小球的运动方向发生改变" +
				"这是因为小球受到与其运动方向不同的侧向磁铁吸引力的缘故。", 510,30);
		label2 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);
		layout1.addChild(label2);
		label2.setColor(cc.color(0, 0, 0, 250));
		label2.setAnchorPoint(1,1);
		label2.setPosition(990,440);

	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
	},
	callback:function(p){
		switch(p.getTag()){		
		case TAG_BUTTON:
			p.setEnable(false);
			//p.setSpriteFrame("sel.png");
			var move = cc.moveTo(1.5,cc.p(600,230));
			var ber = cc.bezierTo(1.5, [cc.p(360,228),cc.p(460,160),cc.p(545,135)]);
			var seq = cc.sequence(cc.callFunc(function(){
				this.line1.setVisible(false);
				this.qiu1.setVisible(false);
				this.label4.setVisible(false);
				this.line2.setVisible(false);
				this.qiu2.setVisible(false);
				this.label5.setVisible(false);
				this.citie.setVisible(false);
			},this),move,cc.callFunc(function(){
				this.line1.setVisible(true);
				this.qiu1.setVisible(true);
				this.label4.setVisible(true);
				this.qiu.setPosition(180,230);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.citie.setVisible(true);
			},this),ber,cc.callFunc(function(){
				this.line2.setVisible(true);
				this.qiu2.setVisible(true);
				this.label5.setVisible(true);
				this.qiu.setPosition(180,230);
				p.setEnable(true);
				//p.setSpriteFrame("unsel.png");
			},this));
			this.qiu.runAction(seq);
			break;
		}
	}
	
});
