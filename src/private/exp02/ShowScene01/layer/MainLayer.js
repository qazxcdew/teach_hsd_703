exp02.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
	
	},
	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);
		
		var text1 = $.format("　　意大利科学家伽利略指出：如果在理想情况下，运动的物体不受到任何阻力，" +
				"则它将保持原来的速度不变，一直以此速度运动下去。" +
				"\n\n　　法国科学家笛卡尔指出：一个运动的物体物体如果不受任何力的作用，不仅速度大小不会改变" +
				"它的运动方向也不会改变。", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setPosition(gg.c_width,gg.height-label1.height/2 - 130);
		this.addChild(label1);	

		var sprite = new cc.Sprite("#niudun.png"); //牛顿
		sprite.setPosition(375,265);
		this.addChild(sprite);

		var text2 = $.format("　　英国物理学家牛顿指出：一切物体在没有受到外力作用时，它们的运动状态保持不变" +
				"包括保持静止或保持匀速直线运动状态——牛顿第一定律", 510,30);
		label2 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);
		this.addChild(label2);
		label2.setAnchorPoint(1,1);
		label2.setColor(cc.color(0, 0, 0, 250));
		label2.setPosition(1125,400);
	}

});
